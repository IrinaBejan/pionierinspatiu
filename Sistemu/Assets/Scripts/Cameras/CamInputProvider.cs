﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
public class CamInputProvider : MonoBehaviour, ICamInputProvider
{
    #region ICamInputProvider Members

    public Vector2 GetCameraOrbitVelocity()
    {
        return orbitVelocity;
    }

    public float GetCameraZoomVelocity()
    {
        return 0.0f;
    }

    public int GetCameraZoomIncrement()
    {
        return zoomIncrement;
    }

    public bool GetCameraChanged()
    {
        return isChanged;
    }

    #endregion


    private Vector2 orbitVelocity;

    private bool isChanged;

    private int zoomIncrement;



    public string orbitHAxis;
  
    public string orbitVAxis;
   
    public string zoomAxis;



    public void Update()
    {
        isChanged = Input.GetKeyDown("c") ? true : false;
        
        zoomIncrement = (zoomAxis != "") ? (int)Input.GetAxis(zoomAxis) : 0;
       
        orbitVelocity = new Vector2((orbitHAxis != "") ? Input.GetAxis(orbitHAxis) : 0.0f, (orbitVAxis != "") ? Input.GetAxis(orbitVAxis) : 0.0f);
    }
}