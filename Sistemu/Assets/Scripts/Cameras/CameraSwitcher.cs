﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Class CameraSwitcher change the current camera in the game with the next one when required, as in a circular linked list
/// </summary>

[DisallowMultipleComponent]
[RequireComponent(typeof(CamInputProvider))]
public class CameraSwitcher : MonoBehaviour
{   
    #region Variables
   
    public ICamInputProvider inputProvider = null;
   
    public Camera[] cams;
    
    private int currentCamera;
    
    private int lastCamera;
  
    #endregion //Variables



    #region MonoBehaviour
    void Awake()
    {
		gameObject.CheckAndInitializeWithInterface<ICamInputProvider>(ref inputProvider);
      
        currentCamera = 0;         
        lastCamera = 0;
        ChangeState(0);
    }

    void Update()
    {
        if (inputProvider.GetCameraChanged())
        {
            lastCamera = currentCamera; 
           
            currentCamera = (currentCamera + 1) % cams.Length;
        }
        else
            lastCamera = currentCamera;

        if (lastCamera != currentCamera)
            ChangeState(currentCamera);
    }
    #endregion //MonoBehaviour

    #region Methods
    void ChangeState(int index)
    {
        foreach (Camera cam in cams)
        {
            cam.gameObject.SetActive(false);
        }

        cams[index].gameObject.SetActive(true);
    }
    #endregion //Methods
}
