﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ICamInputProvider))]
public class FirstPersonCamera : MonoBehaviour
{
    public ICamInputProvider inputProvider = null;
    
    #region Variables
    [SerializeField]
    private Transform followedObject;
    
    [SerializeField]
    private float fpsLookSpeed = 3.0f;
    
    [SerializeField]
    private Vector2 fpsXAxisClamp = new Vector2(-70.0f, 90.0f);
    
    [SerializeField]
    private float fpsRotDegreePerSecond = 120f;
    
    [SerializeField]
    private Vector3 smoothCamVelocity = Vector3.zero;

    private float smoothCamDampTime = 0.1f;
    private float xAxisRot = 0.0f;
    private CameraPosition fpsCamPosition;
    private Vector3 targetPosition;
    private Vector3 lookAt;
    #endregion


    struct CameraPosition
    {
        private Vector3 position;
       
        private Transform cTransform;



        public Vector3 Position { get { return position; } set { position = value; } }
       
        public Transform Transfo { get { return cTransform; } set { cTransform = value; } }



        public void InitialValues(Vector3 pos, Transform transform, Transform parent)
        {
            position = pos;
           
            cTransform = transform;
           
            cTransform.parent = parent;
           
            cTransform.localPosition = Vector3.zero;
            
            cTransform.localPosition = position;
        }
    }

    void Awake()
    {
        gameObject.CheckAndInitializeWithInterface(ref inputProvider);
       
        followedObject = GameObject.FindWithTag("Character").transform;



        fpsCamPosition = new CameraPosition();
       
        fpsCamPosition.InitialValues
            (
                new Vector3(0.0f, 1.6f, 0.2f),
               
                new GameObject().transform,
                
                followedObject
            );
    }

    void LateUpdate()
    {
        Vector2 input = inputProvider.GetCameraOrbitVelocity();
        
        float leftX = input.x;
        
        float leftY = input.y;



        targetPosition = Vector3.zero;
      
        lookAt = Vector3.zero;



        xAxisRot += (leftY * 0.5f * fpsLookSpeed);
       
        xAxisRot = Mathf.Clamp(xAxisRot, fpsXAxisClamp.x, fpsXAxisClamp.y);
       
        fpsCamPosition.Transfo.localRotation = Quaternion.Euler(xAxisRot, 0, 0);
   
        Quaternion rotationShift = Quaternion.FromToRotation(this.transform.forward, fpsCamPosition.Transfo.forward);
       
        this.transform.rotation = rotationShift * this.transform.rotation;



        Vector3 rotationAmount = Vector3.Lerp(Vector3.zero, new Vector3(0f, fpsRotDegreePerSecond * (leftX < 0f ? -1f : 1f), 0f), Mathf.Abs(leftX));
       
        Quaternion deltaRotation = Quaternion.Euler(rotationAmount * Time.deltaTime);
        
        followedObject.rotation = (followedObject.rotation * deltaRotation);

        targetPosition = fpsCamPosition.Transfo.position;




        lookAt = Vector3.Lerp(targetPosition + followedObject.forward, this.transform.position + this.transform.forward, smoothCamDampTime * Time.deltaTime);

        lookAt = (Vector3.Lerp(this.transform.position + this.transform.forward, lookAt, Vector3.Distance(this.transform.position, fpsCamPosition.Transfo.position)));
      
        SmoothPosition(this.transform.position, targetPosition);
      
        transform.LookAt(lookAt);
    }

    private void SmoothPosition(Vector3 startPosition, Vector3 endPosition)
    {

        this.transform.position = Vector3.SmoothDamp(startPosition, endPosition, ref smoothCamVelocity, smoothCamDampTime);
  
    }

    public void OnDrawGizmos()
    {
      
        Debug.DrawRay(Vector3.zero, lookAt, Color.black);
        
        Debug.DrawRay(Vector3.zero, targetPosition + followedObject.forward, Color.white);
        
        Debug.DrawRay(Vector3.zero, fpsCamPosition.Transfo.position + fpsCamPosition.Transfo.forward, Color.cyan);
    
    }
}