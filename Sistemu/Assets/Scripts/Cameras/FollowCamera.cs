﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Class FollowCamera is used to make the volume bounded camera
/// </summary>

[DisallowMultipleComponent]
[RequireComponent(typeof(ITargetFollowPosition))]
public class FollowCamera : MonoBehaviour
{
    public ITargetFollowPosition targetTransform = null;

    public Vector3 Offset
    {
        get{ return offset; }
        set{ offset = value; }
    }

    private Vector3 offset;

    public Bounds boundsVolume;

    public void Awake()
    {
        gameObject.CheckAndInitializeWithInterface<ITargetFollowPosition>(ref targetTransform);

        Offset = transform.position - targetTransform.SmoothedTargetPosition;
    }

    public void LateUpdate()
    {
        if (boundsVolume.size.sqrMagnitude > 0.0f)
            transform.position = Vector3.Max(Vector3.Min(boundsVolume.max, targetTransform.SmoothedTargetPosition + Offset), boundsVolume.min);
        else
            transform.position = targetTransform.SmoothedTargetPosition + Offset;
    }

    public void OnDrawGizmos()
    {
        var col = Gizmos.color;

        Gizmos.color = Color.blue;

        Gizmos.DrawWireCube(boundsVolume.center, boundsVolume.size);

        Gizmos.color = Color.yellow;

        if (targetTransform!= null)
            Gizmos.DrawWireSphere(targetTransform.SmoothedTargetPosition + Offset, 1.0f);

        Gizmos.color = col;
    }
}
