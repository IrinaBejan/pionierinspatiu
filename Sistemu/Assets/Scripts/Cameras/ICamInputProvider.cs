﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface ICamInputProvider
{
    Vector2 GetCameraOrbitVelocity();

    float GetCameraZoomVelocity();

    int GetCameraZoomIncrement();

    bool GetCameraChanged();
}