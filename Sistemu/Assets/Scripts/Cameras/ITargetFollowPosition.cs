﻿using UnityEngine;
using System.Collections;

public interface ITargetFollowPosition
{
    Vector3 SmoothedTargetPosition
    {
        get;
    }
}
