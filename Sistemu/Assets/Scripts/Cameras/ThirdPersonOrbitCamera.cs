﻿using System;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(ICamInputProvider))]
[RequireComponent(typeof(FollowCamera))]
class ThirdPersonOrbitCamera : MonoBehaviour
{
    public ICamInputProvider inputProvider = null;
    public ITargetFollowPosition targetTransform = null;

    public FollowCamera followCameraComponent;

    [Range(0.0f, 10.0f)]
    public float sensitivity = 0.0f;

    public void Awake()
    {
        gameObject.CheckAndInitializeWithInterface<ICamInputProvider>(ref inputProvider);

        gameObject.CheckAndInitializeWithInterface<FollowCamera>(ref followCameraComponent);
    }

    public void FixedUpdate()
    {
        var orbitVelocity = inputProvider.GetCameraOrbitVelocity();
        var offset = followCameraComponent.Offset;

        var radius = offset.magnitude;
        var theta = Mathf.Acos(offset.y / radius);
        var phi = Mathf.Atan2(offset.z, offset.x);

        theta += orbitVelocity.y * Mathf.PI * sensitivity * 1e-2f;
        phi += -orbitVelocity.x * Mathf.PI * sensitivity * 1e-2f;

        theta = Mathf.Clamp(theta, Mathf.PI / 12.0f, Mathf.PI * 11.0f / 12.0f);

        var x = radius * Mathf.Sin(theta) * Mathf.Cos(phi);
        var z = radius * Mathf.Sin(theta) * Mathf.Sin(phi);
        var y = radius * Mathf.Cos(theta);

        followCameraComponent.Offset = new Vector3(x, y, z);
    }
}