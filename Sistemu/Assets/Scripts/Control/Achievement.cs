﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Achievement : MonoBehaviour
{
	public Image badge;
	
	void Start()
	{
		badge = GetComponent<Image>();
	}

	public void Activate()
	{
		badge.color = new Color(255, 255, 255, 255);
	}
}
