﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class AchievementsManager : MonoBehaviour
{
	public string messageForAchievement = "Ai primit o noua distinctie!";
	public List<Achievement> achievements = new List<Achievement>();
	public GameObject main;

	public void UnlockAchievement(int number)
	{
		NotificationsManager.notificationsManager.AddNotification(messageForAchievement);
		achievements[number].Activate();
	}

	void GetChildren()
	{

		main.GetComponentsInChildren<Achievement>(achievements);

	}
}
