﻿using UnityEngine;
using System.Collections;
[DisallowMultipleComponent]
[RequireComponent(typeof(LoadScene))]
public class Back : MonoBehaviour 
{
    public int numberScene;
    public LoadScene sceneLoader;
	void Awake()
    {
        gameObject.CheckAndInitializeWithInterface<LoadScene>(ref sceneLoader);
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
            sceneLoader.LoadNewScene(numberScene);

    }
}
