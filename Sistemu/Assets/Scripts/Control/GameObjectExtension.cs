﻿using UnityEngine;

public static class GameObjectExtension
{
    public static T GetInterface<T>(this GameObject gameObject) where T : class
    {
        return gameObject.GetComponent(typeof(T)) as T;
    }

    public static T[] GetInterfacesInChildren<T>(this GameObject gameObject) where T : class
    {
        return gameObject.GetComponentsInChildren(typeof(T)) as T[];
    }

    public static void CheckAndInitializeWithInterface<T>(this GameObject gameObject, ref T field, bool warn = false) where T : class
    {
        UnityEngine.Object.Equals(null, null);
        if (field == null || field.Equals(null))
        {
            field = gameObject.GetInterface<T>();
        }
    }

    public static T GetInterface<T>(this Component component) where T : class
    {
        return component.GetComponent(typeof(T)) as T;
    }
}
