﻿using System;
using UnityEngine;

public interface IInputProvider
{
    Vector3 GetShipRotation();

    float GetAcceleration();
}
