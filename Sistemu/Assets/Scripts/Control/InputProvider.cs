﻿using UnityEngine;

[DisallowMultipleComponent]
public class InputProvider : MonoBehaviour, IInputProvider
{
    private float yaw;
    private float pitch;
    private float acceleration;
    private float roll = 0.0f;

    #region IInputController

    public Vector3 GetShipRotation()
    {
        return new Vector3(-pitch, yaw, -roll);
    }

    public float GetAcceleration()
    {
        return acceleration;
    }

    #endregion

    #region MonoBehaviour

    void FixedUpdate()
    {
        yaw= Input.GetAxis("Yaw");
       
        pitch = Input.GetAxis("Pitch") ;
        
        acceleration = Input.GetAxis("Acceleration");
    }

    #endregion
}