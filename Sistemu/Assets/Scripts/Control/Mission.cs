﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Assets.Scripts.Control
{
	public class Mission
	{
		public string title;
		public string description;
		public string objectives;
		public int IDImage;
		public string rewards;
		public int points;
		public string tool;
		public string tag;
		public string message;

		public static Mission ConvertTo(XmlNode mission)
		{
			Mission temp = new Mission();
			temp.title = mission.SelectSingleNode("Titlu").InnerText;
			temp.description = mission.SelectSingleNode("Descriere").InnerText;
			temp.IDImage = Convert.ToInt32(mission.SelectSingleNode("IDImage").InnerText);
			temp.objectives = mission.SelectSingleNode("Obiective").InnerText;
			temp.points = Convert.ToInt32(mission.SelectSingleNode("Puncte").InnerText);
			temp.rewards = mission.SelectSingleNode("Recompensa").InnerText;
			temp.message = mission.SelectSingleNode("Mesaj").InnerText;
			temp.tool = mission.SelectSingleNode("Unealta").InnerText;
			temp.tag = mission.SelectSingleNode("Tag").InnerText;
			return temp;
		}
	}
}
