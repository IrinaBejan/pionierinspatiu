﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Xml;
using Assets.Scripts.Control;

public class MissionsManager : MonoBehaviour
{
	public static MissionsManager managerMissions = new MissionsManager();
	public PointsManager points;
	public int[,] levelMissions = new int[5,3]{
																{0, 0,  0},
																{0, 1, 2},
																{3, 4,  4},
																{5, 6,  7},
																{8, 9, 10}
													 };
	public void LevelUpUpdate(int lvl)
	{
		for (int index = 0; index <= 2; index++ )
			if(PlayerInfo.playerData.MissionAvailable.Contains(levelMissions[lvl,index]))
				break;
			else
			{
				AddMission("ID" + levelMissions[lvl, index]);
			}
	}

	public void AddMission(string mission)
	{
		int missionID = GetID(mission);

		if (!PlayerInfo.playerData.MissionAvailable.Contains(missionID))
		{
			PlayerInfo.playerData.MissionAvailable.Add(missionID);

			PlayerInfo.playerData.MissionsNumber = PlayerInfo.playerData.MissionAvailable.Count + 1;

			PlayerInfo.playerData.MissionsNotAccepted = PlayerInfo.playerData.MissionAvailable.Count;
		}
	}

	public void StartMission(string mission)
	{
		int missionID = GetID(mission);

		if (!PlayerInfo.playerData.MissionGoingOn.Contains(missionID))
		{

			PlayerInfo.playerData.MissionAvailable.Remove(missionID);

			PlayerInfo.playerData.MissionGoingOn.Add(missionID);

			PlayerInfo.playerData.MissionsOnGoing = PlayerInfo.playerData.MissionGoingOn.Count + 1;

			PlayerInfo.playerData.MissionsNotAccepted--;

			PlayerInfo.playerData.MissionsAccepted++;
		}
	}

	public void CompleteMission(string mission)
	{
		int missionID = GetID(mission);

		if (PlayerInfo.playerData.MissionGoingOn.Contains(missionID))
		{


			PlayerInfo.playerData.MissionGoingOn.Remove(missionID);

			PlayerInfo.playerData.MissionsOnGoing--;

			PlayerInfo.playerData.MissionArchive.Add(missionID);

			PlayerInfo.playerData.MissionsCompleted++;
		}


	}

	public void GiveUpMission(string mission)
	{
		int missionID = GetID(mission);

		if (PlayerInfo.playerData.MissionGoingOn.Contains(missionID))
		{

			PlayerInfo.playerData.MissionGoingOn.Remove(missionID);

			PlayerInfo.playerData.MissionsOnGoing--;

			PlayerInfo.playerData.MissionArchive.Add(missionID);

			PlayerInfo.playerData.MissionsFailed++;
		}
	}

	public int GetID(string mission)
	{
		try
		{
			return Convert.ToInt32(mission[2]-'0');
		}
		catch 
		{
			Debug.Log("The mission object was not named properly and cannot retrive the ID");
			return 0;
		}
	}

	private XmlDocument DocumentXml = new XmlDocument();
	
	public List<Mission> missionStack  = new List<Mission>();
	
	public void GetMissionsInfos()
	{
		DocumentXml.Load(Application.streamingAssetsPath + "/Missions.xml");
		
		XmlNode root = DocumentXml.SelectSingleNode("Misiuni");
		
		foreach (XmlNode mission in root.SelectNodes("Misiune"))
		
			missionStack.Add(Mission.ConvertTo(mission));
	
	
	}
	

}
