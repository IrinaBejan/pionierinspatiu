﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NotificationsManager : MonoBehaviour 
{
	public static NotificationsManager notificationsManager = new NotificationsManager();
	private System.Collections.Generic.Stack<string> stack = new System.Collections.Generic.Stack<string>();
	public float waitTime = 4.0f;
	public Text notificationsBox;
 

	public void AddNotification(string log)
	{
		stack.Push(log);
		StartCoroutine("DisplayNotifs");
	}

	IEnumerator DisplayNotifs()
	{
		while(stack.Count != 0)
		{
			notificationsBox.text = stack.Pop();
			yield return new WaitForSeconds(waitTime);
		}
		notificationsBox.text = "";
		yield break;
	}

}
