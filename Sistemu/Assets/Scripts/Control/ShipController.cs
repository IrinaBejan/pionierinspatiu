﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class ShipController implements the control of the ship
/// </summary

[DisallowMultipleComponent]
[RequireComponent(typeof(IInputProvider))]
[RequireComponent(typeof(Rigidbody))]
public class ShipController : MonoBehaviour
{
    public float Speed { get; set; }

    #region Variabiles
    
    public Rigidbody shipRigidbody;
    public IInputProvider inputProvider = null;
    
    public float rotationSpeed = 25;
    public float accelerationSpeed = -1f;

    public float distanceKm;
    public float time;

    public float maxSpeed = -400f;
    public float minSpeed = -30f;

    public int pitchConst = 20;
    public int yawConst = 20;
    public int accelerationConst = 20;

    private float pitch;
    private float acceleration;
    private float yaw;
    private float truePitch;
    private float trueYaw;
    #endregion //Variabiles

    #region MonoBehaviour
    public void Awake()
    {

        if (PlayerPrefs.HasKey("PositionX"))
        {
            this.transform.position = new Vector3(PlayerPrefs.GetFloat("PositionX"), PlayerPrefs.GetFloat("PositionY"), PlayerPrefs.GetFloat("PositionZ"));
        }
        else
        { 
            this.transform.position = new Vector3(200.0f, 0.0f, 0.0f);
        }
       

        gameObject.CheckAndInitializeWithInterface(ref inputProvider);
        
        shipRigidbody = gameObject.GetComponent<Rigidbody>();
        
        Speed = 0.0f;
    }

    void Update()
    {
        pitch = inputProvider.GetShipRotation().x * pitchConst;
        
        yaw = inputProvider.GetShipRotation().y * yawConst;
        
        acceleration = inputProvider.GetAcceleration();
        
        if (acceleration > 0) 
            Speed += accelerationSpeed;
        else if (acceleration < 0) 
            Speed -= accelerationSpeed;

        if (Speed < maxSpeed)
            Speed = maxSpeed;
        if (Speed > minSpeed)
            Speed = minSpeed;
        
        pitch *= Time.deltaTime;
        yaw *= -Time.deltaTime;

        truePitch = Mathf.Lerp(truePitch, pitch, rotationSpeed * Time.deltaTime);
        trueYaw = Mathf.Lerp(trueYaw, yaw, rotationSpeed * Time.deltaTime);
    }

    void FixedUpdate()
    {
        Vector3 newPosition = Vector3.forward;
        
        newPosition = shipRigidbody.rotation * newPosition;
        
        shipRigidbody.velocity = newPosition * (Time.deltaTime * Speed);
        
        transform.Rotate(truePitch, -trueYaw, 0);
    }
    #endregion //MonoBehaviour
}