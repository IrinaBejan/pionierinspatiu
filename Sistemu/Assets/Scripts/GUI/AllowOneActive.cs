﻿using UnityEngine;
using System.Collections;
using System;

public class AllowOneActive: MonoBehaviour
{
	public GameObject[] objectsList;

	public void CheckAndClose(GameObject activeObject)
	{
		activeObject.SetActive(true);
		foreach(var obj in objectsList)
			if(obj.GetInstanceID() != activeObject.GetInstanceID() )
			{
				obj.gameObject.SetActive(false);
			}
	}
	
}
