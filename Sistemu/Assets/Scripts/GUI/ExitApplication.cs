﻿using UnityEngine;
using System.Collections;

public class ExitApplication : MonoBehaviour 
{
	public void CloseApplication() 
    {
        Application.Quit();
	}
}
