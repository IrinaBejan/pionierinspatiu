﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class HeightCalculate calculates (or it should) the height of each column representing the evolution per planet
/// </summary>

[RequireComponent(typeof(RectTransform))]
public class HeightCalculate : MonoBehaviour 
{
    public RectTransform proprieties;
   
    public string planet;

    public float posX;


    private float[] hnote = {30.5f, 45f, 80.5f, 105.5f, 130f, 154.5f, 180f, 205.5f, 230.5f, 256f, 280.5f }; //forced solution - to be remade
	
    void Awake()
    {
        proprieties = this.gameObject.GetComponent<RectTransform>();
    }
   
    void Update()
    {
        int note = System.Convert.ToInt32(PlayerPrefs.GetFloat(planet));
       
        if (note <= 0) note = 1;
        
        proprieties.sizeDelta = new Vector2(proprieties.sizeDelta.x, note * 50);
       
        proprieties.localPosition = new Vector3(posX, hnote[note], 0.0f);
    }
}

