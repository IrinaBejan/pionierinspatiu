﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(FadeBetweenScenes))]
public class LoadScene : MonoBehaviour 
{
    public int number = 1;
	public void LoadNewScene(int nr)
    {
        if (nr != -1) number = nr;
        StartCoroutine(Fade());
	}
	
    public IEnumerator Fade()
    {
       float fadeTime = GetComponent<FadeBetweenScenes>().BeginFade(1);
      
       yield return new WaitForSeconds(fadeTime);
        
       Application.LoadLevel(number);
    }
}
