﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class Menu controlls a finite state machine with the responsability of fading in/out the current menu
/// </summary>

public class Menu : MonoBehaviour 
{
 
	public Animator _animator;
	
    public CanvasGroup _canvasGroup;



    #region MonoBehaviour
    public void Awake ()
	{
		_animator = GetComponent<Animator> ();
		
        _canvasGroup = GetComponent<CanvasGroup> ();
		
        var rect = GetComponent<RectTransform> ();
		
        rect.offsetMax = rect.offsetMin = new Vector2 (0, 0);
	}	
 


	public void Update()
	{
		if(!_animator.GetCurrentAnimatorStateInfo(0).IsName("MenuOpen"))
		{
			_canvasGroup.blocksRaycasts = _canvasGroup.interactable = false;
		}
		else
		{
			_canvasGroup.blocksRaycasts = _canvasGroup.interactable = true;
		}
    }
    #endregion //MonoBehaviour



    #region Methods
    public bool IsOpen
    {
        get { return _animator.GetBool("IsOpen"); }
        set { _animator.SetBool("IsOpen", value); }
    }
    #endregion //Methods
}
