﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class MenuManage shows a panel of the canvas at a time by switching on and off the Menu FMS's parameter 
/// </summary>

public class MenuManage : MonoBehaviour 
{
    public Menu CurrentMenu;
   
    public void Start()
    {
        ShowMenu(CurrentMenu);
    }
    
    public void ShowMenu(Menu menu)
    {
        if (CurrentMenu != null)
            CurrentMenu.IsOpen = false;

        
        CurrentMenu = menu;
       
        CurrentMenu.IsOpen = true;
    }
}
