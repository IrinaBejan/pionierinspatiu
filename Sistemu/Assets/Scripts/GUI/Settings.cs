﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Settings : MonoBehaviour 
{
	private float previousTimeScale;
	private bool isGamePaused;

	public Slider vol;
	public void ChangeVolume()
	{
		AudioListener.volume = vol.value ;
	}

	public Text toBeUpdatedQuality;
	public void QualitySet(GameObject button)
	{
		toBeUpdatedQuality.text = button.GetComponentInChildren<Text>().text;
		int index = Convert.ToInt32(button.name);
		QualitySettings.SetQualityLevel(index);
	}

	private readonly int[] width = {1366,1920,1920};
	private readonly int[] height = {768,1080,1200};

	public Text toBeUpdatedResolution;
	public void SetResolution(GameObject button)
	{
		toBeUpdatedResolution.text = button.GetComponentInChildren<Text>().text;
		int index = Convert.ToInt32(button.name);
		Screen.SetResolution(width[index], height[index],false);
	}

	public void Pause()
	{
		previousTimeScale = Time.timeScale;
		Time.timeScale = 0.0f;
		isGamePaused = true;
	}

	public GameObject settingsPanel;
	public void Resume()
	{
		settingsPanel.gameObject.SetActive(false);
		isGamePaused = false;
		Time.timeScale = previousTimeScale;
	}

	public void FullScreen(bool fs)
	{
		Screen.fullScreen = fs;
	}
}
