﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Class ShipController has the responsabilty to calculate and return the board stats (time, distance, speed)
/// </summary

public class StatsBoard : MonoBehaviour
{
    #region Variabiles
    static public float conversion = -621761.65f;
   
    static public float distanceKm;
   
    static public float tTime;
   
    static public float speed;
   
    public GameObject ship;
   
    public ShipController shipo;
  
    #endregion //Variabiles

    #region MonoBehaviour
    private void Awake()
    {
       
        shipo = ship.GetComponent<ShipController>();
      
        if (!PlayerPrefs.HasKey("DistanceKM"))
        {
            distanceKm = 0;
           
            PlayerPrefs.SetFloat("DistanceKM", 0.0f);
           
            PlayerPrefs.SetFloat("Time", 0.0f);
        }
        else
        {
            distanceKm = PlayerPrefs.GetFloat("DistanceKM");
           
            tTime = PlayerPrefs.GetFloat("Time");
        }
    }
    
    void Update()
    {

        speed = shipo.Speed;

        tTime += Time.deltaTime;
        
        distanceKm += shipo.Speed * conversion * Time.deltaTime;
        
        Set("DistanceKM", distanceKm);
        
        Set("Time", tTime);

    }


    void OnCollisionEnter(Collision coll)
    {
        shipo.Speed = 0;
    }

    void OnCollisionExit(Collision coll)
    {
    }

    #endregion 

    #region Methods
    private void Set(string var, float value)
    {
        PlayerPrefs.SetFloat(var, value);
    }

    static public float time()
    {
        return tTime;
    }

    static public float distance()
    {
        return distanceKm/1000;
    }

    static public int speedo()
    {

        return (int)(speed * conversion/1000);
    }
    #endregion //Methods
}
