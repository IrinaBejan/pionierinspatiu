﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/// <summary>
/// Class TextTyping simulates the in time-writing effect when showing the text
/// </summary>
 
public class TextTyping : MonoBehaviour 
{

    public Text eileenText;
    public GameObject interactiveBoard;

    public IEnumerator TypeText(string message, float letterPause)
    {
       
    //    interactiveBoard.gameObject.SetActive(true);
      
        eileenText.text = "";
        
        foreach (char letter in message.ToCharArray())
        {
            eileenText.text += letter;
           
            yield return 0;
            
            yield return new WaitForSeconds(letterPause);
        }

        yield return new WaitForSeconds(8);
     //   interactiveBoard.gameObject.SetActive(false);

    }
}
