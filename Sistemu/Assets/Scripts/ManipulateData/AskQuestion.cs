﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(TextTyping))]
public class AskQuestion : MonoBehaviour 
{
    public ToolsButtons control;

    private int rightNumber;
    private int wrongNumber;

    private int[] order;
    private int correctAns;
    private int currentQuestion;
    private int nrQuestions;

    private string[] inTextQuestions;
    private string[,] inAnswers;

    public Canvas interactiveWorldSpace;
    public Camera boardCamera;
    public Camera backCamera;
    public GameObject textPanel;
	public PointsManager points;

    public GameObject questionPanel;
    public Text question;
    public Toggle[] answers;
    public Text[] answersText;
    public Text eileenText;
    public TextTyping typer;
    float note;
    void Update()
    {
        if (control.Exit == true)
        {
            control.Exit = false;
            StartCoroutine(StartQuiz());
        }
    }

    IEnumerator StartQuiz()
    {
        yield return new  WaitForSeconds(2);
        AskQuestio();
    }

    public void ChangeQuestion()
    {
        if (currentQuestion >= 1)
        {
            int index = 0;

            bool correct = false;


            foreach (Toggle tog in answers)
            {
                if (tog.isOn && index == correctAns)
                {
                    eileenText.text = "Corect!";

                    correct = true;

                    rightNumber++;
                }

                index++;
            }



            if (correct == false)
            {
                eileenText.text = "Hmmm.. ceva e ciudat.";

                wrongNumber++;
            }
            else
            {
                eileenText.text = "Exact cum am banuit !";

                rightNumber++;
            }

        }

        if (currentQuestion == nrQuestions)
        {
            note = rightNumber * 10 / (rightNumber + wrongNumber);

            questionPanel.gameObject.SetActive(false);
            eileenText.text = "Buna treaba ! Cercetatorii spun ca i-ai ajutat in proportie de ";
            eileenText.text += string.Format("{0:0,0.0}", note*10);
            eileenText.text += " la suta!";
			points.AddPoints((int)note * 10);
            StartCoroutine(End());
        }


        order = GenerateRandomPermutation(4);
       
        for (int i = 0; i < order.Length; i++)
            if (order[i] == 3)
               
                correctAns = i;
        
        foreach (Toggle tog in answers)
            tog.isOn = false;

        question.text = inTextQuestions[currentQuestion];

        int j = 0;

        foreach (Text text in answersText)
            text.text = inAnswers[currentQuestion, order[j++]];

        currentQuestion++;
    }


    int[] GenerateRandomPermutation(int length0)
    {
        int[] perm = new int[length0];

        for (int i = 0; i < length0; i++)
        {
            perm[i] = i;
        }

        System.Random random = new System.Random();

        while (length0 > 1)
        {
            length0--;
            int i = random.Next(length0 + 1);
            int temp = perm[i];
            perm[i] = perm[length0];
            perm[length0] = temp;
        }
        return perm;
    }

    IEnumerator End()
    {
        yield return new WaitForSeconds(3);

        PlayerPrefs.SetFloat(control.NameObj, note);

        PlayerPrefs.SetFloat("lastTime", Time.time);
        Finish();
        yield break;
    }

    void Finish()
    {
        textPanel.gameObject.SetActive(false);
    }
    void AskQuestio()
    {
        backCamera.gameObject.SetActive(false);
        
        boardCamera.gameObject.SetActive(true);

        questionPanel.gameObject.SetActive(true);

        textPanel.gameObject.SetActive(true);

        string message = "Astrofizicienii de la statia spatiala au observat ultima ta cercetare si doresc sa le raspunzi la cateva intrebari.";
        
        StartCoroutine(typer.TypeText(message, 0.00001f));
        
        rightNumber = wrongNumber = 0;
        
        control.NameObj = control.LastName;
        
        
        GetInformationXML.getQuestion(control.NameObj, out nrQuestions, out inTextQuestions, out inAnswers);
        
       /* order = GenerateRandomPermutation(4);
        
        for (int i = 0; i < order.Length; i++)
            if (order[i] == 3)
                correctAns = i;

        question.text = inTextQuestions[0];
       
        int j = 0;
        foreach (Text text in answersText)
            text.text = inAnswers[0, order[j++]];
     */
        currentQuestion = 0;
    }
}
