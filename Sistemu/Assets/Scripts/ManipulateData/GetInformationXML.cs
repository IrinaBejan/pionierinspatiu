﻿using UnityEngine;
using System.Collections;
using System.Xml;

/// <summary>
/// Class GetInformationXML extracts the information from the XMLs files
/// </summary>

public class GetInformationXML : MonoBehaviour
{

    static XmlDocument DocumentXml = new XmlDocument();

    static public void getQuestion(string path, out int nrQuestions, out string[] inTextQuestions, out string[,] inAnswers)
    {
        DocumentXml.Load(Application.streamingAssetsPath + "/Planete.xml");
       
        int i = 0, j;
        
        XmlNode root = DocumentXml.SelectSingleNode("Planets");

        inTextQuestions = new string[20];

        inAnswers = new string[20, 20];

        nrQuestions = 0;

        foreach (XmlNode Intrebare in root.SelectNodes(path + "/Intrebari/Intrebare"))
        {
            inTextQuestions[i] = Intrebare.SelectSingleNode("textIntrebare").InnerText;
          
            j = 0;
          
            XmlNodeList pppp = Intrebare.SelectNodes("raspuns");
            
            foreach (XmlNode Raspuns in pppp)
                inAnswers[i, j++] = Raspuns.InnerText;
            
            inAnswers[i, j++] = Intrebare.SelectSingleNode("raspunsBun").InnerText;
            
            i++;
        }

        nrQuestions = i;
    }
}
