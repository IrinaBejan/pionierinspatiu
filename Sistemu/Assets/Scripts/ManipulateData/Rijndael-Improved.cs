﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Implements the Rijindal symmetric encryption algorithm with some tweaks b-)
/// </summary>

public class SecureData
{
	#region Constants
	private static string DEFAULT_HASH_ALGORITHM = "SHA1";
	private static int DEFAULT_KEY_SIZE = 256;
	private static int MAX_ALLOWED_SALT_LEN = 255;
	private static int MIN_ALLOWED_SALT_LEN = 4;
	private static int DEFAULT_MIN_SALT_LEN = MIN_ALLOWED_SALT_LEN;
	private static int DEFAULT_MAX_SALT_LEN = 8;
	private int minSaltLen = -1;
	private int maxSaltLen = -1;

	private ICryptoTransform encryptor = null;
	private ICryptoTransform decryptor = null;
	#endregion

	#region Constructors
    /// <summary>
    /// ECB mode, NEVER EVER USE
	/// </summary>
    public SecureData(string  passPhrase):
        this(passPhrase, null)
    {
    }
	
	public SecureData(string  passPhrase,
                            string  initVector) :
        this(passPhrase, initVector, -1)
    {
    }
 
	public SecureData(string  passPhrase,
                            string  initVector,
                            int     minSaltLen) :
        this(passPhrase, initVector, minSaltLen, -1)
    {
    }

	public SecureData(string  passPhrase,
                            string  initVector,
                            int     minSaltLen,
                            int     maxSaltLen) :
        this(passPhrase, initVector, minSaltLen, maxSaltLen, -1)
    {
    }

    public SecureData(string  passPhrase,
                            string  initVector,
                            int     minSaltLen,
                            int     maxSaltLen,
                            int     keySize) :
        this(passPhrase, initVector, minSaltLen, maxSaltLen, keySize, null)
    {
    }
	
	public SecureData(string  passPhrase,
                            string  initVector,
                            int     minSaltLen,
                            int     maxSaltLen, 
                            int     keySize,
                            string  hashAlgorithm) : 
        this(passPhrase, initVector, minSaltLen, maxSaltLen, keySize, 
             hashAlgorithm, null)
    {
    }

    public SecureData(string  passPhrase,
                            string  initVector,
                            int     minSaltLen,
                            int     maxSaltLen, 
                            int     keySize,
                            string  hashAlgorithm,
                            string  saltValue) : 
        this(passPhrase, initVector, minSaltLen, maxSaltLen, keySize, 
             hashAlgorithm, saltValue, 1)
    {
    }
 
    public SecureData(string  passPhrase,
                            string  initVector,
                            int     minSaltLen,
                            int     maxSaltLen, 
                            int     keySize,
                            string  hashAlgorithm,
                            string  saltValue,
                            int     passwordIterations)
    {
        if (minSaltLen < MIN_ALLOWED_SALT_LEN)
            this.minSaltLen = DEFAULT_MIN_SALT_LEN;
        else
            this.minSaltLen = minSaltLen;
 
        if (maxSaltLen < 0 || maxSaltLen > MAX_ALLOWED_SALT_LEN)
            this.maxSaltLen = DEFAULT_MAX_SALT_LEN;
        else
            this.maxSaltLen = maxSaltLen;
 
        if (keySize <= 0)
            keySize = DEFAULT_KEY_SIZE;
 
        if (hashAlgorithm == null)
            hashAlgorithm = DEFAULT_HASH_ALGORITHM;
        else
            hashAlgorithm = hashAlgorithm.ToUpper().Replace("-", "");
 
        byte[] initVectorBytes = null;
        byte[] saltValueBytes  = null;

        if (initVector == null)
            initVectorBytes = new byte[0];
        else
            initVectorBytes = Encoding.ASCII.GetBytes(initVector);
 
		if (saltValue == null)
            saltValueBytes = new byte[0];
        else
            saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

		Rfc2898DeriveBytes password = new Rfc2898DeriveBytes(
                                                   passPhrase,
                                                   saltValueBytes,
                                                   passwordIterations);
 
		byte[] keyBytes = password.GetBytes(keySize / 8);
        RijndaelManaged symmetricKey = new RijndaelManaged();
 
        if (initVectorBytes.Length == 0)
            symmetricKey.Mode = CipherMode.ECB;
        else
            symmetricKey.Mode = CipherMode.CBC;
 
        encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
        decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
    }
    #endregion
 
    #region Encryption routines
    public string Encrypt(string plainText)
    {
        return Encrypt(Encoding.UTF8.GetBytes(plainText));
    }
 
    public string Encrypt(byte[] plainTextBytes)
    {
        return Convert.ToBase64String(EncryptToBytes(plainTextBytes));
    }

    public byte[] EncryptToBytes(string plainText)
    {
        return EncryptToBytes(Encoding.UTF8.GetBytes(plainText));
    }

    public byte[] EncryptToBytes(byte[] plainTextBytes)
    {
        byte[] plainTextBytesWithSalt = AddSalt(plainTextBytes);
 
        MemoryStream memoryStream = new MemoryStream();
        
        lock (this)
        {
            CryptoStream cryptoStream = new CryptoStream(
                                               memoryStream, 
                                               encryptor,
                                                CryptoStreamMode.Write);
 
			cryptoStream.Write( plainTextBytesWithSalt, 
                                0, 
                               plainTextBytesWithSalt.Length);
             
            cryptoStream.FlushFinalBlock();
 
			byte[] cipherTextBytes = memoryStream.ToArray();
               
            memoryStream.Close();
            cryptoStream.Close();
 
            return cipherTextBytes;
        }
    }
    #endregion
 
    #region Decryption routines

    public string Decrypt(string cipherText)
    {
        return Decrypt(Convert.FromBase64String(cipherText));
    }
 
    public string Decrypt(byte[] cipherTextBytes)
    {
        return Encoding.UTF8.GetString(DecryptToBytes(cipherTextBytes));
    }
 
    public byte[] DecryptToBytes(string cipherText)
    {
        return DecryptToBytes(Convert.FromBase64String(cipherText));
    }
 
    
    public byte[] DecryptToBytes(byte[] cipherTextBytes)
    {
        byte[] decryptedBytes      = null;
        byte[] plainTextBytes      = null;
        int    decryptedByteCount  = 0;
        int    saltLen             = 0;
 
        MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
 
        // When using CBC, cipher text is always longer than the plain text and cause we don't know the length, we take the cipher text's length.
        decryptedBytes = new byte[cipherTextBytes.Length];
 
        lock (this)
        {
            CryptoStream  cryptoStream = new CryptoStream(
                                               memoryStream,
                                               decryptor,
                                               CryptoStreamMode.Read);
 
            decryptedByteCount  = cryptoStream.Read(decryptedBytes,
                                                    0, 
                                                    decryptedBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
        }

        if (maxSaltLen > 0 && maxSaltLen >= minSaltLen)
        {
            saltLen =   (decryptedBytes[0] & 0x03) |
                        (decryptedBytes[1] & 0x0c) |
                        (decryptedBytes[2] & 0x30) |
                        (decryptedBytes[3] & 0xc0);
        }

        plainTextBytes = new byte[decryptedByteCount - saltLen];
 
        Array.Copy(decryptedBytes, saltLen, plainTextBytes, 
                    0, decryptedByteCount - saltLen);
 
        return plainTextBytes;
    }
    #endregion
 
    #region Helper functions
    private byte[] AddSalt(byte[] plainTextBytes)
    {
        if (maxSaltLen == 0 || maxSaltLen < minSaltLen)
            return plainTextBytes;
 
        byte[] saltBytes = GenerateSalt();
        byte[] plainTextBytesWithSalt = new byte[plainTextBytes.Length +
                                                 saltBytes.Length];
        Array.Copy(saltBytes, plainTextBytesWithSalt, saltBytes.Length);

        Array.Copy( plainTextBytes, 0, 
                    plainTextBytesWithSalt, saltBytes.Length,
                    plainTextBytes.Length);
 
        return plainTextBytesWithSalt;
    }

    private byte[] GenerateSalt()
    {
        int saltLen = 0;

        if (minSaltLen == maxSaltLen)
            saltLen = minSaltLen;
        else
            saltLen = GenerateRandomNumber(minSaltLen, maxSaltLen);
 
        byte[] salt = new byte[saltLen];
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        rng.GetNonZeroBytes(salt);
		//The first four bytes of the salt array will contain the salt length split into four two-bit pieces.
        salt[0] = (byte)((salt[0] & 0xfc) | (saltLen & 0x03));
        salt[1] = (byte)((salt[1] & 0xf3) | (saltLen & 0x0c));
        salt[2] = (byte)((salt[2] & 0xcf) | (saltLen & 0x30));
        salt[3] = (byte)((salt[3] & 0x3f) | (saltLen & 0xc0));
 
        return salt;
    }

    private int GenerateRandomNumber(int minValue, int maxValue)
    {
		///Why ? Cause .NET Framework's Random class, called multiple times within a very short period of time, generates the same "random" number.
        byte[] randomBytes = new byte[4];
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        rng.GetBytes(randomBytes);
 
        int seed = ((randomBytes[0] & 0x7f) << 24) |
                    (randomBytes[1]         << 16) |
                    (randomBytes[2]         << 8 ) |
                    (randomBytes[3]);
 
        Random  random  = new Random(seed);
 
        return random.Next(minValue, maxValue+1);
    }
    #endregion
}
 