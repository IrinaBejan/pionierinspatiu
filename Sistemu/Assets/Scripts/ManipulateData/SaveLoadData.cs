﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoadData
{
	public static List<PlayerInfo> savedGames = new List<PlayerInfo>();
	public static SaveLoadData dataManage;

	#region Security
	private readonly string passPhrase = "e5jHAU294Dhsuh1dna";
	private readonly string initVector = "@1B2c3D4e5F6g7H8";
	SecureData rijndaelKey = new SecureData(dataManage.passPhrase, dataManage.initVector);
	#endregion

	public void Save(PlayerInfo current)
	{
		savedGames.Add(current);
		using (var fileStream = File.Create(Application.persistentDataPath + "/info.astro"))
		using (var memoryStream = new MemoryStream())
		{
			var formatter = new BinaryFormatter();
			formatter.Serialize(memoryStream, SaveLoadData.savedGames);

			memoryStream.Seek(0, SeekOrigin.Begin);

			var bytes = new byte[memoryStream.Length];
			memoryStream.Read(bytes, 0, (int)memoryStream.Length);
			
			var encryptedBytes = rijndaelKey.EncryptToBytes(bytes);
			fileStream.Write(encryptedBytes, 0, encryptedBytes.Length);
		}
	}

	public void Load(PlayerInfo current)
	{
		if (File.Exists(Application.persistentDataPath + "/info.astro"))
		{
			using(FileStream file = File.Open(Application.persistentDataPath + "/info.astro", FileMode.Open))
			{
				byte[] bytes = new byte[file.Length];
				int numBytesToRead = (int)file.Length;
				int numBytesRead = 0;

				while (numBytesToRead > 0)
				{
	                int n = file.Read(bytes, numBytesRead, numBytesToRead);
				    if (n == 0)
					    break;
					numBytesRead += n;
					numBytesToRead -= n;
				}
				var decryptedBytes = rijndaelKey.DecryptToBytes(bytes);
				using(MemoryStream memoryStream = new MemoryStream(decryptedBytes))
				{
					BinaryFormatter bf = new BinaryFormatter();	
					SaveLoadData.savedGames = (List<PlayerInfo>)bf.Deserialize(memoryStream);
				}
			}
		}
		else
		{
			Debug.Log("Destination file " + Application.persistentDataPath + "/info.astro was not found");
		}
	}
}
