﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine;

public class AllPlanets
{
	enum PlanetNames { Sun, Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto, Moon, EarthTopo };
	
	#region Variabiles
	public Planetoid[] plan = new Planetoid[12];
	private String pathToDEFiles = Application.streamingAssetsPath;
	#endregion //Variabiles

	#region Constructors
	public AllPlanets()
	{
		plan[(int)PlanetNames.Sun] = new Planetoid();
		plan[(int)PlanetNames.Mercury] = new Planetoid();
		plan[(int)PlanetNames.Venus] = new Planetoid();
		plan[(int)PlanetNames.Earth] = new Planetoid();
		plan[(int)PlanetNames.Mars] = new Planetoid();
		plan[(int)PlanetNames.Jupiter] = new Planetoid();
		plan[(int)PlanetNames.Saturn] = new Planetoid();
		plan[(int)PlanetNames.Uranus] = new Planetoid();
		plan[(int)PlanetNames.Neptune] = new Planetoid();
		plan[(int)PlanetNames.Pluto] = new Planetoid();
		plan[(int)PlanetNames.Moon] = new Planetoid();
		plan[(int)PlanetNames.EarthTopo] = new Planetoid();

		for (int i = 0; i < 12; i++)
		{
			plan[i].EclipCart = new double[4];
		}

		plan[(int)PlanetNames.Sun].Name = "Sun";
		plan[(int)PlanetNames.Mercury].Name = "Mercury";
		plan[(int)PlanetNames.Venus].Name = "Venus";
		plan[(int)PlanetNames.Earth].Name = "Earth";
		plan[(int)PlanetNames.Mars].Name = "Mars";
		plan[(int)PlanetNames.Jupiter].Name = "Jupiter";
		plan[(int)PlanetNames.Saturn].Name = "Saturn";
		plan[(int)PlanetNames.Uranus].Name = "Uranus";
		plan[(int)PlanetNames.Neptune].Name = "Neptune";
		plan[(int)PlanetNames.Pluto].Name = "Pluto";
		plan[(int)PlanetNames.Moon].Name = "Moon";
		plan[(int)PlanetNames.EarthTopo].Name = "EarthTopo";
	}
	#endregion //Constructors

	#region Methods
	internal bool SetPath(String PathTo)
	{
		pathToDEFiles = PathTo;
		if (File.Exists(pathToDEFiles + "/elp82.dat") && File.Exists(pathToDEFiles + "/ps_1996.dat")) return true;
		return false;
	}
	#endregion //Methods
}
