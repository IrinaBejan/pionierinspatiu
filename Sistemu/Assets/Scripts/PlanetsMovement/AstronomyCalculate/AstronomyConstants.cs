﻿using System.Collections;

public static class AstronomyConstants
{
	#region Constants
	public const double J2000 = 2451545.0;
    public const double AU_IN_KM = 1.49597870691e+8;
    public const double AU_IN_METERS = 1.49597870691e+11;
    public const double SPEED_OF_LIGHT = 299792.458;
    public const double PI = 3.14159265358979323846264338327950288;
	#endregion 
}
