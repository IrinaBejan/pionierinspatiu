﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// Calculates the planetary position in a system raported to Earth. It uses the magic PS 1996 theory.
/// </summary>
public class DE_Planets
{
	 enum PlanetNames { Sun, Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto, Moon, EarthTopo };
	 const double R2S = 206264.806247111668690598;

	 #region Variabiles
	 static double LastPlanetJD = 0.0;
     static double[] earthVector = new double[6];
     static double[] xyz = new double[4];

	 #endregion

	 #region Methods
	 public static void PlanetMain(int planetIndex, double julienDate, String path, AllPlanets AllP)
	 {

		 double[] stateVector = new double[6];
		 BinaryReader PDataFile = null;
		 Poisson poisson = new Poisson();

		 int i;
		 double rightAscension = 0; double declination = 0;
		 double distb;
		 FileStream fs = null;
		 String FileName;
		 if (julienDate != LastPlanetJD)
		 {
			 LastPlanetJD = julienDate;
	
			 const double PI = 3.14159265358979323846264338327950288;
			 const double EARTH_MOON_BALANCE = 81.3;

			 julienDate += DeltaTimeUTCTD.DeltaTimeCompute(julienDate) / 86400.0;

			 //Cause PS1996 doesn't include the earth's position, in the following code we compute the earthVector using EMB
			 FileName = path + "/ps_1996.dat";
			 try
			 {
				 fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
				 PDataFile = new BinaryReader(fs);
			 }
			 catch
			 {
				 Debug.Log("ps_1996.dat file not found !!!");
				 return;
			 }
			 poisson = PlanetaryEfems.LoadPS1996Series(PDataFile, julienDate, 3);
			 PlanetaryEfems.GetPS1996Position(julienDate, poisson, earthVector, 0);
			 distb = GetRaDec(earthVector, ref rightAscension, ref declination);
			 CalculateRates(AllP, (int)PlanetNames.Earth, julienDate, rightAscension, declination, distb);


			 AllP.plan[(int)PlanetNames.Earth].EclipCart[0] = earthVector[0];
			 AllP.plan[(int)PlanetNames.Earth].EclipCart[1] = earthVector[1];
			 AllP.plan[(int)PlanetNames.Earth].EclipCart[2] = earthVector[2];

			 for (i = 0; i < 3; i++)
				 earthVector[i] -= xyz[i] / (EARTH_MOON_BALANCE * AstronomyConstants.AU_IN_KM);
			 distb = GetRaDec(earthVector, ref rightAscension, ref declination);
			 AllP.plan[(int)PlanetNames.Earth].julienDate = julienDate;
			 AllP.plan[(int)PlanetNames.Earth].Distance = distb;
			 AllP.plan[(int)PlanetNames.Earth].rightAscension = rightAscension;
			 AllP.plan[(int)PlanetNames.Earth].declination = declination;

			 double rightAscension2 = rightAscension - PI; 
			 if (rightAscension2 < 0) 
				 rightAscension2 += PI * 2;
			 double de2 = 0 - declination;
			 CalculateRates(AllP, (int)PlanetNames.Sun, julienDate, rightAscension2, de2, distb);

		 }

		 if (PDataFile == null)
		 {
			 FileName = path + "/ps_1996.dat";
			 try
			 {
				 fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
				 PDataFile = new BinaryReader(fs);
			 }
			 catch
			 {
				 Debug.Log("ps_1996.dat  file not found !!!");
				 return;
			 }
		 }
		 if (planetIndex == (int)PlanetNames.Earth || planetIndex == (int)PlanetNames.Moon || planetIndex == (int)PlanetNames.Sun)
		 {
			 fs.Close();
			 return;
		 }

		 const int N_PASSES = 3;
		 if (planetIndex != 3)
		 {
			 double dist = 0.0;
			 double[] delta = new double[3];
			 int pass;

			 poisson = PlanetaryEfems.LoadPS1996Series(PDataFile, julienDate, planetIndex);
			 for (pass = 0; pass < N_PASSES; pass++)
			 {
				 PlanetaryEfems.GetPS1996Position(julienDate - dist * AstronomyConstants.AU_IN_KM / (AstronomyConstants.SPEED_OF_LIGHT * 86400.0), poisson, stateVector, 0);
				 dist = 0.0;
				 for (i = 0; i < 3; i++)
				 {
					 delta[i] = stateVector[i] - earthVector[i];
					 dist += delta[i] * delta[i];
				 }
				 dist = Math.Sqrt(dist);
			 }
			 distb = GetRaDec(delta, ref rightAscension, ref declination);
			 CalculateRates(AllP, planetIndex, julienDate, rightAscension, declination, distb);
		 }
	 }

	 static double GetRaDec(double[] vect, ref double rightAscension, ref double declination)
	 {
		 rightAscension = Math.Atan2(vect[1], vect[0]);
		 double dist = Math.Sqrt(vect[0] * vect[0] + vect[1] * vect[1] + vect[2] * vect[2]);
		 declination = Math.Asin(vect[2] / dist);
		 if (rightAscension < 0.0) rightAscension += AstronomyConstants.PI * 2.0;
		 double rat = rightAscension * 12 / AstronomyConstants.PI;
		 double dect = declination * 180 / AstronomyConstants.PI;
		 if (dect == rat) dect = rat;

		 return (dist);
	 }

	 static void CalculateRates(AllPlanets AllP, int Which, double jd, double rightAscension, double de, double distb)
	 {
		 double TimeDiff = jd - AllP.plan[Which].julienDate;
		 double rightAscensionDiff = rightAscension - AllP.plan[Which].rightAscension;
		 double declinationDiff = de - AllP.plan[Which].declination;
		 AllP.plan[Which].julienDate = jd;
		 AllP.plan[Which].Distance = distb;
		 AllP.plan[Which].rightAscension = rightAscension;
		 AllP.plan[Which].declination = de;

		 TimeDiff = TimeDiff * 86400;   // 1440 * 60;
		 if (Math.Abs(TimeDiff) > 0.2 && Math.Abs(TimeDiff) < 60)
		 {
			 double Rate = (rightAscensionDiff * R2S / TimeDiff);
			 AllP.plan[Which].rightAscensionRate = Rate;
			 Rate = (declinationDiff * R2S / TimeDiff);
			 AllP.plan[Which].declinationRate = Rate;
		 }

	 }
	 #endregion 
}
