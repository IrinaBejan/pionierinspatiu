﻿using System.Collections;

/// <summary>
/// Computes Delta T - between Universal time and dinamical time - for time system conversion. Computing the earth's rotation requires us to use UT.  But the moon and planets move without regard to the earth's rotation;  for them,  TD is used.
/// </summary>

public class DeltaTimeUTCTD
{
	#region Constants
	/// For dates between 1620 and 2010,  the value comes from the 'deltaTimeTable' using data from the USNO sites referenced below.  The table gives delta-t at two-year intervals;  between those values,  a linear interpolation is used.  If the year is before 1620,  one of two quadratic approximations is used.   If the date is after the end of the table, a linear extrapolation is used,  with a quadratic term added to that which assumes an acceleration of 32.5 seconds/century. 
	/// ftp://maia.usno.navy.mil/ser7/deltat.data
	/// ftp://maia.usno.navy.mil/ser7/deltat.preds
	const int DELTA_T_TABLE_SIZE = 196;
	static readonly short[] deltaTimeTable = new short[DELTA_T_TABLE_SIZE]
 {12400, 11500, 10600, 9800, 9100, 8500, 7900,  /*  1620-1632 */
   7400, 7000, 6500, 6200, 5800, 5500, 5300,    /*  1634-1646 */
   5000, 4800, 4600, 4400, 4200, 4000, 3700,    /*  1648-1660 */
   3500, 3300, 3100, 2800, 2600, 2400, 2200,    /*  1662-1674 */
   2000, 1800, 1600, 1400, 1300, 1200, 1100,    /*  1676-1688 */
   1000,  900,  900,  900,  900,  900,  900,    /*  1690-1702 */
    900,  900, 1000, 1000, 1000, 1000, 1000,    /*  1704-1716 */
   1100, 1100, 1100, 1100, 1100, 1100, 1100,    /*  1718-1730 */
   1100, 1200, 1200, 1200, 1200, 1200, 1300,    /*  1732-1744 */
   1300, 1300, 1300, 1400, 1400, 1400, 1500,    /*  1746-1758 */
   1500, 1500, 1500, 1600, 1600, 1600, 1600,    /*  1760-1772 */
   1600, 1700, 1700, 1700, 1700, 1700, 1700,    /*  1774-1786 */
   1700, 1700, 1600, 1600, 1500, 1400, 1370,    /*  1788-1800 */
   1310, 1270, 1250, 1250, 1250, 1250, 1250,    /*  1802-1814 */
   1250, 1230, 1200, 1140, 1060,  960,  860,    /*  1816-1828 */
    750,  660,  600,  570,  560,  570,  590,    /*  1830-1842 */
    620,  650,  680,  710,  730,  750,  770,    /*  1844-1856 */
    780,  790,  750,  640,  540,  290,  160,    /*  1858-1870 */
   -100, -270, -360, -470, -540, -520, -550,    /*  1872-1884 */
   -560, -580, -590, -620, -640, -610, -470,    /*  1886-1898 */
   -270,    0,  260,  540,  770, 1050, 1340,    /*  1900-1912 */
   1600, 1820, 2020, 2120, 2240, 2350, 2390,    /*  1914-1926 */
   2430, 2400, 2390, 2390, 2370, 2400, 2430,    /*  1928-1940 */
   2530, 2620, 2730, 2820, 2910, 3000, 3070,    /*  1942-1954 */
   3140, 3220, 3310, 3400, 3500, 3650, 3830,    /*  1956-1968 */
   4020, 4220, 4450, 4650, 4850, 5050, 5220,    /*  1970-1982 */
   5380, 5490, 5580,                            /*  1984-1988 */
  5686,											/* 1990 */
  5831,											/* 1992 */
  5998,											/* 1994 */
  6163,											/* 1996 */
  6297,											/* 1998 */
  6383,											/* 2000 */
  6430,											/* 2002 */
  6457,											/* 2004 */
  6485,											/* 2006 */
  6546,											/* 2008 */
  6607 };										/* 2010 */
	#endregion 

	#region Methods
	public static double DeltaTimeCompute(double julienDate)
	{
		double year, dinamicalTime, result = 0;

		/// get the year and the dinamical time out of julien date
		year = 2000.0 + (julienDate - 2451545.0) / 365.25;
		dinamicalTime = (year - 2000.0) / 100.0;

		if (year < 948.0)
			result = 2715.6 + dinamicalTime * (573.36 + 46.5 * dinamicalTime);
		else if (year < 1620.0)
			result = 50.6 + dinamicalTime * (67.5 + 22.5 * dinamicalTime);
		else
		{
			double indexPlace = (year - 1620.0) / 2.0;
			int index = (int)indexPlace;

			if (index > DELTA_T_TABLE_SIZE - 2)
				index = DELTA_T_TABLE_SIZE - 2;
			dinamicalTime = indexPlace - (double)index;
			result = (double)deltaTimeTable[index + 0] + (double)(deltaTimeTable[index + 1] - deltaTimeTable[index + 0]) * dinamicalTime;
			result /= 100.0;
			if (dinamicalTime > 1.0)
			{
				dinamicalTime = (dinamicalTime - 1.0) / 50.0;    /* cvt to centuries with the add of the 32.5 sec/cy^2 used by Stephenson*/
				result += 32.5 * dinamicalTime * dinamicalTime;
			}
		}
		return (result);
	}
	/* Niels Bohr noted that "prediction is hard,  especially about the future." This is especially true for predicting Delta-T. Some assumed that the rate of change of Delta-T can be determined from the last two entries in the above 'deltaTimeTable' array, with a quadratic term of 32.5 seconds/century^2 added to this.  This is likely to provide good "near-term" results.
	*/
	#endregion 
}


