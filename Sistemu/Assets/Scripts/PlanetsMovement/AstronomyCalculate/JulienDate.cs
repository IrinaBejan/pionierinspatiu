﻿using System.Collections;
using System;

public class JulienDate
{
	#region Variabiles
	private static DateTime UT = new DateTime();
	#endregion

	#region Methods
	public static double GetCurrentJulDay()
    {
        DateTime ut = DateTime.UtcNow;
        double Y = ut.Year;
        double M = ut.Month;
        double D = ut.Day;
        if (M <= 2) { Y--; M += 12; }
        double A = Math.Floor(Y / 100.0);
        double B = 2 - A + Math.Floor(A / 4);
        double C = Math.Floor(365.25 * Y);
        double E = Math.Floor(30.6001 * (M + 1.0));
        double julienDate = B + C + D + E + 1720994.5;

        double Hr = ut.Hour / 24.0;
        double Min = ut.Minute / 1440.0;
        double mS = ut.Millisecond;
        double Sec = (ut.Second / 86400.0) + ((mS / 1000) / 86400.0);
        julienDate += (Hr + Min + Sec);
        return julienDate;
    }
       
    public static DateTime JulDayToUT(double julienDate)
    {
        double dayfrac, JulianDay;
        int l, n, yr, mn;

        JulianDay = Math.Floor(julienDate + 0.5);
        dayfrac = (julienDate + 0.5) - JulianDay;

        int hh = (int)(24.0 * dayfrac);
        int mm = (int)(60.0 * (24.0 * dayfrac - hh));
        int ss = (int)((60.0 * (24.0 * dayfrac - hh) - mm) * 60.0);

        l = (int)(JulianDay) + 68569;
        n = 4 * l / 146097;

        l = l - (146097 * n + 3) / 4;
        yr = 4000 * (l + 1) / 1461001;

        l = l - 1461 * yr / 4 + 31;  
        mn = 80 * l / 2447;

        int day = (int)(l - 2447 * mn / 80);
        l = mn / 11;
        int month = (int)(mn + 2 - 12 * l);
        int year = (int)(100 * (n - 49) + yr + l);
        UT = new DateTime(year, month, day, hh, mm, ss);
        return UT;
	}
	#endregion

}
