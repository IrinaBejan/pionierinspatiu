﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;

/// <summary>
/// Calculates planets positions using the  PS1996 algorithm and the output(the right ascension and declination) are in  J2000 coordinates 
/// </summary>
public class PlanetPositions : MonoBehaviour
{
	#region Constants
	enum PlanetNames { Sun, Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto, Moon, EarthTopo };
	const double PI = 3.14159265358979323846264338327950288;
	internal const double D2R = 0.01745329251994;
	#endregion 

	#region Variabiles
	private string LBJulDay, LBPlanetData; //test variabiles
	private double julianToday = 25000.0;
	Planets planets;
	#endregion 

	#region MonoBehaviour
	void Start()
	{
		String pathToEmphemerisFiles = Application.streamingAssetsPath;
		
		planets = new Planets(pathToEmphemerisFiles);

		julianToday = JulienDate.GetCurrentJulDay();
		planets.CalculateAllPositions(julianToday);
		PutDataToString();
	}
	#endregion

	public static string[] rightAscension = new string[10];
	public static string[] declination = new string[10];
	#region Methods
	private void PutDataToString()
	{
		rightAscension[0] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Sun].rightAscension);
		declination[0] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Sun].declination);

		rightAscension[1] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Mercury].rightAscension);
		declination[1] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Mercury].declination);

		rightAscension[2] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Venus].rightAscension);
		declination[2] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Venus].declination);

		rightAscension[4] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Mars].rightAscension);
		declination[3] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Mars].declination);

		rightAscension[5] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Jupiter].rightAscension);
		declination[5] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Jupiter].declination);

		rightAscension[6] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Saturn].rightAscension);
		declination[6] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Saturn].declination);

		rightAscension[7] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Uranus].rightAscension);
		declination[7] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Uranus].declination);

		rightAscension[8] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Neptune].rightAscension);
		declination[8] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Neptune].declination);

		rightAscension[9] = StringTransform.Rads2Timedp(planets.All.plan[(int)PlanetNames.Pluto].rightAscension);
		declination[9] = StringTransform.Rads2Degsdp(planets.All.plan[(int)PlanetNames.Pluto].declination);
	}

	public void ShowPosition(Text text)
	{
		if(text.name != "Terra")
			text.fontSize = 16;

		if (text.name == "Mercur")
			text.text = "RA: "+ rightAscension[1] + " Dec: " + declination[1];
		
		if (text.name == "Venus")
			text.text = "RA: " + rightAscension[2] + " Dec: " + declination[2];

		if (text.name == "Mars")
			text.text = "RA: " + rightAscension[4] + " Dec: " + declination[4];

		if (text.name == "Jupiter")
			text.text = "RA: " + rightAscension[5] + " Dec: " + declination[5];

		if (text.name == "Saturn")
			text.text = "RA: " + rightAscension[6] + " Dec: " + declination[6];

		if (text.name == "Uranus")
			text.text = "RA: " + rightAscension[7] + " Dec: " + declination[7];

		if (text.name == "Neptune")
			text.text = "RA: " + rightAscension[8] + " Dec: " + declination[8];

	}

	public Vector3 PolarToCartesian(float r, float rightAscension, float declination)
	{
		float x, y, z;

		x = r * Mathf.Cos(rightAscension) * Mathf.Sin(Mathf.PI - declination);
		y = r * Mathf.Sin(rightAscension) * Mathf.Sin(Mathf.PI - declination);
		z = r * Mathf.Cos(Mathf.PI - declination);

		return new Vector3(x, y, z);
	}

	public void HidePosition(Text text)
	{
		text.fontSize = 22;
		text.text = text.name;
	}
	#endregion 
}
