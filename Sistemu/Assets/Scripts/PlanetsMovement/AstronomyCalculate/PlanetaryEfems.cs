﻿using System.IO;
using System.Collections;
using System;

/// <summary>
/// Functions for planetary ephemerides from PS-1996 theory - implemented similar to what is written in Pluto Project to get the data.
/// </summary>
public class PlanetaryEfems
{
	#region Methods
	/// <summary>
	/// In the PS_1996.DAT file,  the coefficients are stored in sets of six doubles.  They're small values and they can be stored in a single byte. Eithewwise, a int16_t or int32_t is needed or in some cases, they need a full eight-byte double. To handle this,  a set of six doubles is stored starting with a int16_t integer;  it is parsed out as six pairs of bits.  Each pair of bits tells you if a given coefficient is stored as a char,  int16_t	int,  or full double.  Finally,  the number of bytes that were parsed is returned.
	/// </summary>
	static int UnpackSixDoubles(ref double[] ovals, BinaryReader ibuff, int group)
	{
		int i;
		int flags = ibuff.ReadInt16();
		int Addr = group * 6;
		for (i = 0; i < 6; i++, flags >>= 2)
			switch (flags & 3)
			{
				case 0:
					ovals[i + Addr] = (double)ibuff.ReadByte();
					break;
				case 1:
					ovals[i + Addr] = (double)ibuff.ReadInt16();
					break;
				case 2:
					ovals[i + Addr] = (double)ibuff.ReadInt32();
					break;
				case 3:
					ovals[i + Addr] = (double)ibuff.ReadDouble();
					break;
			}
		return 0;
	}

	/// <summary>
	/// This function loads up the ps1996 series for a given planet.  A buffer of ten int32_t integers is read in;  this provides the offset within the file for the data concerning each planet.  Some checking is done to figure out which 'block' covers the current julienDate;  of course, it's possible that none will,  in which case we close up shop and return a NULL.
	/// 
	/// Otherwise,  we figure out how much memory is needed and allocate one massive buffer to hold it all.  This may look a little odd,  but it serves several purposes.  First,  you only have to check one memory allocation for success or failure,  which simplifies the code a little. Second,  freeing up the buffer when you're done with it is much easier  I use this "allocate one buffer" trick a lot. And so, we read the data.
	/// </summary>
	public static Poisson LoadPS1996Series(BinaryReader ifile, double julienDate, int planetIndex)
	{
		int offset;
		int jump = 0;
		int read_error = 0;
		short[] block_sizes;
		int block, i;

		PoissonHeader header = new PoissonHeader();
		Poisson poisso = new Poisson();

		ifile.BaseStream.Seek((planetIndex - 1) * 4, SeekOrigin.Begin);

		offset = ifile.ReadInt32();
		ifile.BaseStream.Seek(offset, SeekOrigin.Begin);
		header.tzero = ifile.ReadDouble();
		header.dt = ifile.ReadDouble();
		header.nf[0] = ifile.ReadInt16();
		header.nf[1] = ifile.ReadInt16();
		header.nf[2] = ifile.ReadInt16();
		header.n_blocks = ifile.ReadInt16();
		header.total_fqs = ifile.ReadInt16();

		block = (int)Math.Floor((julienDate - header.tzero) / header.dt);

		if (block < 0 || block >= header.n_blocks || read_error != 0)
		{
			return poisso;
		}

		poisso.tzero = header.tzero + (double)block * header.dt;
		poisso.dt = header.dt;
		poisso.n_blocks = header.n_blocks;
		poisso.total_fqs = header.total_fqs;
		for (i = 0; i < 3; i++)
			poisso.nf[i] = header.nf[i];

		poisso.fqs = new double[header.total_fqs];
		for (int ii = 0; ii < header.total_fqs; ii++)
		{
			poisso.fqs[ii] = ifile.ReadDouble();
		}

		block_sizes = new short[header.n_blocks];
		for (int ii = 0; ii < header.n_blocks; ii++)
		{
			block_sizes[ii] = ifile.ReadInt16();
		}

		for (i = 0; i < block; i++) jump += (int)block_sizes[i];

		ifile.BaseStream.Seek(jump, SeekOrigin.Current);

		for (int ii = 0; ii < 12; ii++) poisso.secular[ii] = ifile.ReadDouble();

		poisso.terms = new double[poisso.total_fqs * 6];
		for (i = 0; i < poisso.total_fqs; i++)
			UnpackSixDoubles(ref poisso.terms, ifile, i);

		return poisso;
	}

	/// <summary>
	///   The function first makes sure that the Poisson-series data pointer you passed in,  iptr,  actually covers the julienDate you passed in;  if it doesn't,  you get a return value of -1.0  If all is well,  it goes through and computes the Cartesian position (x, y, z) of the object, in AU,  on the date in question,  in heliocentric J2000 coordinates. 
	/// x = stateVector[0], y = stateVector[1], z = stateVector[2].  If you pass in a non-zero value for computeVelocity,  then it will also figure out the velocity,  in AU/day,  as 
	/// vx = stateVector[3],  vy = stateVector[4],	vz = stateVector[5].
	/// </summary>
	/// 
	internal static int GetPS1996Position(double julienDate, Poisson p, double[] stateVector, int computeVelocity)
	{
		double x = 2.0 * (julienDate - p.tzero) / p.dt - 1.0;
		double fx = x * p.dt / 2.0;

		int i, j, m;
		double wx = 1.0;
		double[] xpower = new double[5];

		if (julienDate < p.tzero || julienDate > p.tzero + p.dt)
			return (-1);
		xpower[0] = xpower[1] = 1.0;
		int secPtrC = 0;
		for (i = 2; i < 5; i++)
			xpower[i] = xpower[i - 1] * x;
		for (i = 0; i < 3; i++)
		{
			stateVector[i] = 0.0;
			wx = 1.0;
			for (j = 0; j < 4; j++)
			{
				if (computeVelocity != 0)
				{
					if (j != 0)
						stateVector[i + 3] += (double)j * (p.secular[secPtrC]) * xpower[j];
					else
						stateVector[i + 3] = 0.0;
				}
				stateVector[i] += wx * (p.secular[secPtrC++]);
				wx *= x;
			}
			if (computeVelocity != 0)
				stateVector[i + 3] *= 2.0 / p.dt;
		}

		wx = 1.0;
		int fqPtrC = 0;
		int pDtermsC = 0;
		for (m = 0; m < 3; m++)
		{
			double[] newSums = new double[6];
			double velocityScale = (double)(m * 2) * xpower[m] / p.dt;

			for (i = 0; i < 6; i++)
				newSums[i] = 0.0;
			for (j = 0; j < p.nf[m]; j++)
			{
				double amplitude = p.fqs[fqPtrC++];
				double f = amplitude * fx;
				double cosTerm = Math.Cos(f);
				double sinTerm = Math.Sin(f);

				for (i = 0; i < 3; i++, pDtermsC += 2)
				{
					newSums[i] += p.terms[pDtermsC + 0] * cosTerm + p.terms[pDtermsC + 1] * sinTerm;
					if (computeVelocity != 0)
						newSums[i + 3] += amplitude *
								   (p.terms[pDtermsC + 1] * cosTerm - p.terms[pDtermsC + 0] * sinTerm);
				}
			}

			for (i = 0; i < 3; i++)
			{
				stateVector[i] += newSums[i] * wx;
				if (computeVelocity != 0)
				{
					stateVector[i + 3] += newSums[i + 3] * wx;
					if (m != 0) stateVector[i + 3] += velocityScale * newSums[i];
				}
			}
			wx *= x;
		}

		for (i = 0; i < ((computeVelocity != 0) ? 6 : 3); i++)
			stateVector[i] *= 1.0e-10;         /* cvt to AU,  and to AU/day */
		return (0);
	}
	#endregion 
}
