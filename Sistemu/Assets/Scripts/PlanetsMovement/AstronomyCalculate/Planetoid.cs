﻿using System.Collections;

public class Planetoid
{
     public string Name = "";
     public double julienDate;
     public double rightAscension;
     public double declination;
     public double Distance;
     public double rightAscensionRate;
     public double declinationRate;
     public double[] EclipCart = new double[4];
     public double Phase;
}