﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Planets
{
	#region Variabiles
	public AllPlanets All = new AllPlanets();
	public static String pathToDEFiles = Application.streamingAssetsPath;
	#endregion

	#region Constructors
	public Planets(String pathTo)
	{
		All = new AllPlanets();
		All.SetPath(pathTo);
	}
	#endregion

	#region Methods
	public void CalculateAllPositions(double julienDate)
	{
		for (int i = 0; i < 11; i++)
		{
			DE_Planets.PlanetMain(i, julienDate, pathToDEFiles, All);
		}
	}
	#endregion
}