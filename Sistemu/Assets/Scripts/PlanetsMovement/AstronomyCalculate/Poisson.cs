﻿using UnityEngine;
using System.Collections;

public class Poisson
{
     public double tzero, dt;
	 public short n_blocks, total_fqs;    /* mx, imax are both = 2 in all cases... */
	 public double[] secular = new double[12];
	 public double[] fqs;  //was a pointer
	 public double[] terms; //was a pointer
	 public int[] nf = new int[3];
}
