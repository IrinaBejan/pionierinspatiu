﻿using UnityEngine;
using System.Collections;

public class PoissonHeader
{
	 internal double tzero, dt;
     internal short[] nf = new short[3];
     internal short n_blocks, total_fqs;
}
