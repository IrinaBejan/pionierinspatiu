﻿using UnityEngine;
using System.Collections;
using System;

public class StringTransform : MonoBehaviour
{
	#region Constants
	const double R2D = 57.29577951308;
    const double PT2 = 6.28318530718;
    const double R2H = 3.8197186342054;
	#endregion 

	#region Methods
	public static String Rads2Timedp(double ts)
	{
		String h, m, s;
		if (ts < 0.0) { ts = 0.0 - ts; h = "-"; }
		else h = "";
		while (ts > PT2) ts -= PT2;

		double hrs = Math.Floor(ts * R2H);
		double mins = Math.Floor(((ts * R2H) - hrs) * 60.0);
		if (mins > 59.99999) { mins = 0.0; hrs += 1.0; }
		double secs = ts * R2H * 60.0 - hrs * 60.0 - mins;
		secs = Math.Round(secs, 1);
		if (secs >= 60.0)
		{
			mins++;
			if (mins > 59.99999) { mins = 0.0; hrs++; }
			if (hrs >= 24.0) hrs = 0.0;
		}
		if (hrs < 10.0) h = h + "0";
		h = h + Convert.ToString(hrs); h = h + ":";
		if (mins < 10.0) m = "0"; else m = "";
		m = m + Convert.ToString(mins); m = m + ":";
		if (secs < 10.0) s = "0"; else s = "";
		s = s + Convert.ToString(secs);
		if (secs == Math.Floor(secs)) s = s + ".0";
		return h + m + s;
	}

	public static String Rads2Degsdp(double ds)
	{
		String d, m, s;
		if (ds < 0.0) { ds = 0.0 - ds; d = "-"; }
		else d = " ";
		while (ds > PT2) ds -= PT2;

		double degs = Math.Floor(ds * R2D);
		double mins = Math.Floor(((ds * R2D) - degs) * 60.0);
		if (mins > 59.99999) { mins = 0.0; degs += 1.0; }
		double secs = ds * R2D * 60.0 - degs * 60.0 - mins;
		secs = Math.Round(secs * 60.0, 1);
		if (secs > 59.99999)
		{
			secs = 0.0;
			mins = mins + 1.0;
			if (mins > 59.99999) { mins = 0.0; degs = degs + 1.0; }
		}
		if (degs < 10.0) d = d + "0";
		d = d + Convert.ToString(degs); d = d + ":";
		if (mins < 10.0) m = "0"; else m = "";
		m = m + Convert.ToString(mins); m = m + ":";
		if (secs < 10.0) s = "0"; else s = "";
		s = s + Convert.ToString(secs);
		if (s.Length < 3) s = s + ".0";
		return d + m + s;
	}
	#endregion

}
