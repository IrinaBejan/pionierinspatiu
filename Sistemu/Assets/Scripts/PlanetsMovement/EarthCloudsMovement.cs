﻿using UnityEngine;
using System.Collections;

public class EarthCloudsMovement : MonoBehaviour 
{
    public float rotationSpeed = 0.001f;
    private float zRotation = 0;
	void Update () 
    {
        zRotation += rotationSpeed;
        Mathf.Clamp(zRotation, 0, 360);
        transform.eulerAngles = new Vector3(0, zRotation, 0);
	}
}
