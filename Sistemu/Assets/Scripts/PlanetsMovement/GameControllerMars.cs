﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameControllerMars : MonoBehaviour
{
    #region Variabiles
    public ParticleSystem dustStorm;

    public string message;
   
    public float letterPause = 0.1f;
    
    public Text eileenText;
    
    public InputField age;



    protected bool startEnded = false;
  
    protected bool vikingEnded = false;
    
    protected bool galeEnded= false;
   
    protected bool inputWait = false;
    #endregion //Variabiles

    #region Monobehaviour
    IEnumerator Start () 
    {
        QualitySettings.vSyncCount = 1;
        message = "Ma bucur sa vad ca ai aterizat cu bine pe Marte. Sper ca nu ai avut o calatorie dificila.";
	    StartCoroutine(TypeText(eileenText));
        yield return new WaitForSeconds(message.Length * (letterPause+0.1f) + 2.0f);

        startEnded = true;
        yield break;
    }

    void OnCollisionEnter(Collision hit)
    {
        if (hit.collider.tag == "SpaceObject" && startEnded)
        {
            startEnded = false;

            StartCoroutine(Viking());
        }

        if (hit.collider.tag == "Gale" && vikingEnded)
        {
            vikingEnded = false;

            StartCoroutine(Gale());
        }
    }

    #endregion //Monobehaviour

    #region Methods
    IEnumerator TypeText(Text text)
    {
        text.text = "";
       
        foreach (char letter in message.ToCharArray())
        {
            text.text += letter;
            
            yield return 0;
            
            yield return new WaitForSeconds(letterPause);
        }
    }

    IEnumerator Viking()
    {

        vikingEnded = true;
       
        yield break;
    }

    IEnumerator Gale()
    {
        dustStorm.gameObject.SetActive(true);

        message = "Oh nu, suntem in pericol ! Trebuie sa parasim planeta cat mai curand. Aici au loc cele mai puternice furtuni de nisip din sistemul solar. Ele apar cand Marte este in pozitia cea mai apropiata de Soare, mai exact, chiar acum !!!!!";
        letterPause /= 10;
        StartCoroutine(TypeText(eileenText));
        yield return new WaitForSeconds(message.Length * (letterPause+0.1f) + +3.0f);
        
        Application.LoadLevel(1);
    }
    #endregion //Methods
}
