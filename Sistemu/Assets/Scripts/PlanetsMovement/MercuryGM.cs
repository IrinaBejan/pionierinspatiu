﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Xml;
using System;

public class MercuryGM : MonoBehaviour 
{
    public Text eileenText;
    public float letterPause;
	public Button next;
	private int messageIndex = 0;
	private bool isWriting = false;
	private List<string> message = new List<string>() { "Hey, bine ai venit pe Mercur!" };
	public PointsManager pManager;

	void Start()
	{
		WriteNumber();
	}

	public void WriteNumber()
    {
		 eileenText.text = "";
		 StartCoroutine(TypeText(eileenText, message[messageIndex].ToCharArray()));
		 messageIndex++;
		 if (messageIndex == message.Count) next.interactable = false;
	}

	public void ToolPicked(GameObject tool)
	{
		var name = tool.name;
		if( PlayerInfo.playerData.MissionGoingOn.Contains(2) && name == "ElectricalScrewDriverProbe")
		{
			string messageMission = MissionsManager.managerMissions.missionStack[2].message;

			message.Add(messageMission);
			WriteNumber();
			pManager.AddPoints(MissionsManager.managerMissions.missionStack[2].points);
			MissionsManager.managerMissions.CompleteMission("ID2");
		}
	}

    #region Methods
    IEnumerator TypeText(Text text, char[] messag)
    {
		StartWriting();
        text.text = "";

        foreach (char letter in messag)
        {
			if (isWriting == false) yield break;
            text.text += letter;
			if (text.text.Length > 200 && letter == ' ')
				text.text = "";

            yield return 0;

            yield return new WaitForSeconds(letterPause);
        }
		StopWriting();
    }

	void StartWriting()
	{
		next.interactable = false;
		isWriting = true;
	}

	void StopWriting()
	{
		if(messageIndex != message.Count)
			next.interactable = true;
		isWriting = false;
	}
    #endregion //Methods
}
