﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VenusGameMaster : MonoBehaviour 
{
    public Text eileenText;
    public float letterPause;
    readonly string[] message =  {
    "Hey, bine ai venit pe Venus, desi aici nu e extraordinar de bine..",
	"Cu peisajul deshidratant, rosu-portocaliu si temperaturile la suprafata suficient de fierbinti pentru a topi plumbul, Venus este analogul sistemului nostru solar pentru iad. Instalarea unei baze acceptabile pe planeta asta ar fi o incercare cu mult dincolo de capacitatile noastre tehnologice acum, dar cam asta ar fi viata aici daca am putea trai de fapt pe Venus.",
    "Si totusi, odata Venus nu era asa. Nu stiu daca iti poti imagina avand in vedere ce vezi acum. Desi cele doua planete erau asemanatoare, Venus a evoluat in timp intr-un mod complet diferit. De ce? Haide sa aflam impreuna.",
    "Vanturile solare au distrus treptat moleculele de apa din atmosfera Venusului, lasand in urma doar gaze si dioxid de carbon (95%). Dioxidul de carbon genereaza un val de caldura imens, datorita faptului ca capteaza caldura soarelui, si realizeaza un fenomen numit efectul de sera, ce a ridicat temperatura la suprafata Venusului la 462 grade Celsius. ",
	"Aerul este atat de gros incat, daca incerci sa muti bratul repede, vei simti rezistenta. Daca nu ai avea acum acest costum special, ai fi rapus de temepratura si de presiunea foarte mare, similara cu presiunea apei de pe Pamant la aproximativ la o jumatate de mila (1 km) sub ocean.",
	"Efectul de sera de altfel, a dus la disparitia apei de pe Venus, transformandul, dupa cum vezi, intr-o zona dominata de depresiuni inalte si munti imensi, precum Maxwell Montes, dar si foarte mult vulcani, unii inca activi, 70% din suprafata fiind lava.",
    "Si de ce totusi pamantul nu a ajuns la fel ? Efectul de sera este un fenomen existent si la noi, defapt toate aspectele privitoare la incalzirea globala au fost descoperite pe Venus in 1999, cand s-a vazut unde poate ajunge Pamantul datorita emisiilor gazoase si mai ales de CO2 in atmosfera.",
    "Desi cantitati mari de gaze in atmosfera pot avea efecte oribile, fara a avea aceste gaze, tot Pamanatul ar ingheta, ca si alte planete din sistemul solar, caci aceste gaze capteaza caldura de la soare si o mentin pentru a permite viata.",
    "Sigur te intrebi cum acum de ce vanturile solare nu ne-a afectat atmosfera la fel ca si Venus, desi apar si la noi, ramanand doar la latitudinea noastra cum ne ingrijim atmosfera. ",
    "Pai, treaba sta in felul urmator : Pamantul se roteste foarte repede fata de Venus, o zi pe Venus fiind echivalentul a 243 de zile ale noastre. De aceea, Pamantul creeaza in jurul lui un camp magnetic mult mai puternic, respingand vanturile solare ce se propaga in jurul polilor, acele frumoase aureole boreale pe care le poti vedea daca te intorci pe Pamant !",
	"Cam asta ar fi viata pe Venus, comparativ cu Venusul. Dar daca ai vrea sa te plangi la prieteni acasa despre cum lava ti-a distrus curtea, nu te astepta un raspuns imediat",
	"mesajul tau ar lua cateva minute pentru a ajunge pe Pamant atunci cand cele doua planete se afla la distanta cea mai scurta. Si cand Venus este de cealalta parte a soarelui de pe Pamant, ar putea dura aproximativ 15 minute pentru mesaj sa ajunga acasa.",
    "Stiu bine ce gandesti. Nicaieri nu-i ca acasa si sunt sigura ca Venusul chiar ti-a starnit dorul de casa. Asa ca eu zic sa mergem, dar esti mere binevenit inapoi sa descoperi mai multe despre Venus. ",
    "El nu a fost mult studiat, datorita condiitilor ostile, doar radare si roboti putand oferi informatii despre acesta. Astfel, sunt multe intrebari deschise la care poti indrazni sa cauti raspunsul,",
	"intrebari ce ne pot ajuta sa aflam mai multe despre propria planeta, efectul de sera, si cum va arata Pamantul urmasiilor nostri.",
    };


	public bool eileenWriting = default(bool);
	public int currentMessageBeingWritten = 0;
	void Start()
    {
        QualitySettings.vSyncCount = 1;
        StartCoroutine(TypeText(eileenText));
	}

	public void ShowNextMessage()
	{
		if (!eileenWriting)
		{
			currentMessageBeingWritten++;
			StartCoroutine(TypeText(eileenText));
		}

	}

	#region Methods
	IEnumerator TypeText(Text text)
	{
		eileenWriting = true;

		text.text = "";

		foreach (char letter in message[currentMessageBeingWritten].ToCharArray())
		{
			text.text += letter;

			yield return 0;

			yield return new WaitForSeconds(letterPause);
		}
		eileenWriting = false;
	}

	#endregion //Methods
}
