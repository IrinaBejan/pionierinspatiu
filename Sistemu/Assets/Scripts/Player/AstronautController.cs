﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
[RequireComponent(typeof(Locomotion))]
[RequireComponent(typeof(AstronautFController))]
[RequireComponent(typeof(IAstronautInputProvider))]

public class AstronautController : MonoBehaviour
{
    #region Public Variabiles
    public Locomotion locomotionController = null;
	
    public AstronautFController characterPhysicsController = null;
	
    public IAstronautInputProvider inputProvider = null;
	
    public Camera characterCamera = null;

    public enum EInputDirectionalMode
	{
		WorldRelative,
        CameraRelative,
		CharacterRelative,
	}
	public EInputDirectionalMode inputDirectionalMode;
    #endregion //Public Variabiles


    #region MonoBehaviour
    public void Awake()
	{
        gameObject.CheckAndInitializeWithInterface<Locomotion>(ref locomotionController);

        gameObject.CheckAndInitializeWithInterface<AstronautFController>(ref characterPhysicsController);

        gameObject.CheckAndInitializeWithInterface<IAstronautInputProvider>(ref inputProvider);


		if (characterCamera == null)
			characterCamera = Camera.main;
	}


	public void Update()
	{
		Vector3 inputVelocity = inputProvider.GetCharacterInputVelocity();

		switch(inputDirectionalMode)
		{
			case EInputDirectionalMode.CameraRelative:
				locomotionController.TargetVelocity = characterCamera.transform.TransformDirection(inputVelocity); 
				break;
			
            case EInputDirectionalMode.CharacterRelative:
				locomotionController.TargetVelocity = transform.TransformDirection(inputVelocity);
				break;
			
            case EInputDirectionalMode.WorldRelative:
				locomotionController.TargetVelocity = inputVelocity;
				break;
			
            default:
				break;
		}
	}
	#endregion //MonoBehaviour
}
