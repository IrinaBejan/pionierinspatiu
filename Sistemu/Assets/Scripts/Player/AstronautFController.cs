﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The class AstronautFController controls the physics - movement of a character
/// </summary>

[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public class AstronautFController : MonoBehaviour 
{
    public float extraGravityFactor = 1.25f;

    protected bool isWalking;
    protected bool isRunning;

    protected Animator animator;
    protected Rigidbody characterRigidbody;

    public void Awake()
    {
        gameObject.CheckAndInitializeWithInterface(ref characterRigidbody);
       
        gameObject.CheckAndInitializeWithInterface(ref animator);
    }

    public float ForwardSpeed
    {
        get { return Vector3.Dot(characterRigidbody.velocity, transform.forward); }
    }

    public Vector3 CurrentVelocity
    {
        get { return GetComponent<Rigidbody>().velocity; }
    }

    protected void FixedUpdate()
    {
        characterRigidbody.AddForce(Physics.gravity * extraGravityFactor, ForceMode.Acceleration);
    }
}
