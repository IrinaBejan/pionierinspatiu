﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class AstronautInputProvider : MonoBehaviour, IAstronautInputProvider
{
    public string ForwardAxis;
  
    public string RightAxis;
  
    private Vector3 inputVelocity = Vector3.zero;


    #region ICharacterInputProvider
    public Vector3 GetCharacterInputVelocity()
    {
        return inputVelocity;
    }
    #endregion //ICharacterInputProvider

   
    #region MonoBehaviour
    public void Update()
    {
        inputVelocity.z = Input.GetAxis(ForwardAxis);
     
        inputVelocity.x = Input.GetAxis(RightAxis);
    }
    #endregion //MonoBehaviour

    #region Methods
    private void ResetInputState()
    {
        inputVelocity = Vector3.zero;
    }
    #endregion //Methods

}