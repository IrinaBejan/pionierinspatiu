﻿using UnityEngine;
using System.Collections;

public interface IAstronautInputProvider
{
    Vector3 GetCharacterInputVelocity();
}
