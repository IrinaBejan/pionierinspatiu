﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[System.Serializable]
public class PlayerInfo
{
	public static PlayerInfo playerData = new PlayerInfo();
	[SerializeField]
	private float? volume;
	[SerializeField]
	private int? quality;
	[SerializeField]
	private bool? fullscreen;
	[SerializeField]
	private Resolution resolution;

	public float? Volume
	{
		set { volume = value; }
		get { if(!volume.HasValue) volume = 1; return volume.Value; }
	}

	public Resolution Resolution
	{
		set { resolution = value; }
		get { if(resolution.Equals(null)) resolution = Screen.currentResolution; return resolution; }
	}

	public int? Quality
	{
		set { quality = value; }
		get { if (!quality.HasValue) quality = QualitySettings.GetQualityLevel(); return quality.Value; }
	}

	public bool? Fullscreen
	{
		set { fullscreen = value;}
		get { if (!fullscreen.HasValue) fullscreen = Screen.fullScreen; return fullscreen.Value; }	
	}

	[SerializeField]
	private List<int> missionsArchive = new List<int>();
	[SerializeField]
	private List<int> missionsAvailable = new List<int>();
	[SerializeField]
	private List<int> missionsGoingOn = new List<int>();

	public List<int> MissionArchive
	{
		set { missionsArchive = value; }
		get { return missionsArchive;  }
	}

	public List<int> MissionGoingOn
	{
		set { missionsGoingOn = value; }
		get { return missionsGoingOn; }
	}

	public List<int> MissionAvailable
	{
		set { missionsAvailable = value; }
		get { return missionsAvailable; }
	}

	[SerializeField]
	private int level = 0;
	[SerializeField]
	private int experience = 0;

	public int Level
	{
		set {  level = value; }
		get
		{

			if (level == 0)
			{
				level = 1;
				MissionsManager.managerMissions.LevelUpUpdate(1);
			}
			return level;
		}
	}

	public int Experience
	{
		set { experience = value; }
		get { if(experience == 0) experience = 0; return experience; }
	}

	[SerializeField]
	private int? missionsAccepted;

	[SerializeField]
	private int? missionsFailed;

	[SerializeField]
	private int? missionsOnGoing;

	[SerializeField]
	private int? missionsCompleted;

	[SerializeField]
	private int? missionsNumber;

	[SerializeField]
	private int? missionsNotAccepted;

	public int? MissionsAccepted
	{
		get
		{
			if (!missionsAccepted.HasValue) missionsAccepted = 0; return missionsAccepted;
		}
		set
		{
			missionsAccepted = value;
		}
	}

	public int? MissionsFailed
	{
		get
		{
			if (!missionsFailed.HasValue) missionsFailed = 0;  return missionsFailed;
		}
		set
		{
			missionsFailed = value;
		}
	}

	public int? MissionsOnGoing
	{
		get
		{
			if (!missionsOnGoing.HasValue) missionsOnGoing = 0; return missionsOnGoing;
		}
		
		set
		{
			missionsOnGoing = value;
		}
	}

	public int? MissionsCompleted
	{
		get
		{
			if (!missionsCompleted.HasValue) missionsCompleted = 0; return missionsCompleted;
		}
		
		set
		{
			missionsCompleted = value;
		}
	}

	public int? MissionsNotAccepted
	{
		get
		{
			if (!missionsNotAccepted.HasValue) missionsNotAccepted = 0; return missionsNotAccepted;
		}
		
		set
		{
			missionsNotAccepted = value;
		}
	}
	public int? MissionsNumber
	{
		get
		{
			if (!missionsNumber.HasValue) missionsNumber = 0;  return missionsNumber;
		}
		
		set
		{
			missionsNumber = value;
		}
	}

}
