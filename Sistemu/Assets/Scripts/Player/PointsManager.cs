﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour 
{
	private int level;
	
	private int experience;
	
	private readonly int[] expRequire = {0, 400, 1000, 1700, 2500, 3400, 4400, 5500, 6700, 8000, 9400, 10900 };


	public Text levelShow;
	
	public Text experienceShow;
	
	public Slider experiencePercentage;
	
	public NotificationsManager notifBox;



	public void Start()
	{
		UpdateGUI();
	}

	private void UpdateGUI()
	{
		level = PlayerInfo.playerData.Level;

		levelShow.text = "Lvl." + level.ToString();

		experience = PlayerInfo.playerData.Experience;
		experienceShow.text = "Experienta:" + experience.ToString();
		

		experiencePercentage.value = (float) experience / expRequire[level];
	}


	public void AddPoints(int numberPoints)
	{
		level = (int) PlayerInfo.playerData.Level;
	
		experience = (int) PlayerInfo.playerData.Experience;
		
		experience += numberPoints;
		
		if (numberPoints != 0)
			notifBox.AddNotification("+" + numberPoints.ToString() + " puncte !");
		
		while(experience > expRequire[level])
		{
			level++;
		
			PlayerInfo.playerData.Level = level;
			
			notifBox.AddNotification("Level nou: " + level + "!!!"); 
		}

		PlayerInfo.playerData.Experience = experience;
		
		UpdateGUI();
	}

}
