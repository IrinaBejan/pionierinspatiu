﻿using UnityEngine;
using System.Collections;

public class RootLocomotion : Locomotion
{

	protected Animator animator;
    [SerializeField]
    private string speedAnimatorParamName = "MoveSpeed";

	private int speedAnimatorParamHash;

    private float moveSpeedDeltaTimeLerpFactor = 4f;

	protected float speedFactor;

	protected float targetMoveSpeedFactor;

	protected Vector3 targetMoveDirection = Vector3.zero;



    protected void Awake()
    {
		gameObject.CheckAndInitializeWithInterface(ref animator);

		AnimatorSpeedParamName = speedAnimatorParamName;
    }

	public string AnimatorSpeedParamName
	{
		get 
        {
            return speedAnimatorParamName; 
        }
		set 
		{
			speedAnimatorParamName = value;
			
            AnimatorSpeedParamHash = Animator.StringToHash(speedAnimatorParamName);
		}
	}

	public int AnimatorSpeedParamHash 
	{
		get { return speedAnimatorParamHash; }
		
        protected set { speedAnimatorParamHash = value; }
	}
	
    public float MoveSpeedDeltaTimeLerpFactor
    {
        get { return moveSpeedDeltaTimeLerpFactor; }
      
        set { moveSpeedDeltaTimeLerpFactor = value; }
    }

    public Animator LocomotionAnimator
    {
        get { return animator; }
    }

	public override Vector3 TargetVelocity 
	{
		set 
		{
			base.TargetVelocity = value;

			float velocityMagnitude = targetVelocity.magnitude;
			
            targetMoveSpeedFactor = Mathf.Clamp01(velocityMagnitude);

			if (velocityMagnitude > 1E-5f)
				targetMoveDirection = targetVelocity / velocityMagnitude;
			else
				targetMoveDirection = Vector3.zero;
		}
	}
	public override void UpdateTurnLogic()
	{
		if (targetMoveDirection != Vector3.zero)
		{
	    	transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.LookRotation(targetMoveDirection), Time.deltaTime * TurnSpeedFactorTowardTarget);
		}
	}

    public override void UpdateMoveLogic()
    {
        speedFactor = Mathf.Lerp(speedFactor, targetMoveSpeedFactor, Time.deltaTime * moveSpeedDeltaTimeLerpFactor);
       
        LocomotionAnimator.SetFloat(AnimatorSpeedParamHash, speedFactor);
    }
}

