﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Class ShipPositioning makes sure to place the ship in the same place it was in last session, or if it's the case, the place of teleportation;
/// </summary>
[RequireComponent(typeof(TextTyping))]
[RequireComponent(typeof(LoadScene))]
public class ShipPositioning : MonoBehaviour 
{
    public GameObject[] planets;
    
    public GameObject ship;
   
    private TextTyping typer;
   
    private LoadScene sceneLoader;

    readonly Vector3[] positions =
    {
        new Vector3(58.74f, -0.86f, -0.34f),
        new Vector3(95.19f, -1.89f, -0.27f),
        new Vector3(130.94f, -2.71f, -1.14f),
        new Vector3(198.28f, -1.41f, -0.45f),
        new Vector3(685.1f, -17.3f, -7.9f),
        new Vector3(1260.3f, -35f, -21.5f),
        new Vector3(2480.14f, -0.884f, -0.464f),
        new Vector3(3892.23f, -3.66f, -0.24f),

    };
	void Start()
    {
        gameObject.CheckAndInitializeWithInterface<TextTyping>(ref typer);
     
        gameObject.CheckAndInitializeWithInterface<LoadScene>(ref sceneLoader);


        if (!PlayerPrefs.HasKey("PositionX"))
        {
            StartCoroutine(FirstMessage());
          
            PlayerPrefs.SetFloat("PositionX", 200.0f);
            
            PlayerPrefs.SetFloat("PositionY", 0.0f);
            
            PlayerPrefs.SetFloat("PositionZ", 0.0f); 
        }
        else
        {
            if (PlayerPrefs.HasKey("Teleportation"))
            {
                if (PlayerPrefs.GetInt("Teleportation") == 1)
                {
                    PlayerPrefs.SetInt("Teleportation", 0);
                    
                    Teleportate(PlayerPrefs.GetInt("Target"));
                }
            }
        }
	}

    IEnumerator FirstMessage()
    {
        string message;
    
        yield return new WaitForSeconds(5);
       

        message = "Bine ai venit ! Dupa cum stii, de-a lungul timpului, omenirea s-a straduit sa cerceteze nemarginitul spatiul. Astazi, este datoria ta sa continui acest datuum !";
       
        StartCoroutine(typer.TypeText(message, 0.0001f));
        
        yield return new WaitForSeconds(message.Length*0.3f);
        
        yield break;
    }

    private void Teleportate(int index)
    {
        PlayerPrefs.SetFloat("PositionX", positions[index].x);

        PlayerPrefs.SetFloat("PositionY", positions[index].y);

        PlayerPrefs.SetFloat("PositionZ", positions[index].z);



        ship.transform.position = new Vector3(PlayerPrefs.GetFloat("PositionX"), PlayerPrefs.GetFloat("PositionY"), PlayerPrefs.GetFloat("PositionZ"));
      
        sceneLoader.LoadNewScene(1);
    }
}
