﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
/// <summary>
/// Class Teleportation teleportates the player in the right place
/// </summary>

public class Teleportation : MonoBehaviour
{
    public int number;
	public void Teleportate()
    {
        PlayerPrefs.SetInt("Teleportation", 1);
        PlayerPrefs.SetInt("Target", number);
        PlayerPrefs.Save();
        Application.LoadLevel(1);
    }

    public GameObject[] planets;

    public void Teleportate(Text t)
    {
        PlanetList.gameObject.SetActive(false);
        name = t.name;
        for (int i = 0; i < 8;i++  )
                if (name == planets[i].name)
                {
                    number = i;
                    Teleportate();
                }
    }

	public void Teleportate(string t)
	{
		PlanetList.gameObject.SetActive(false);
		for (int i = 0; i < 8; i++)
			if (t == planets[i].name)
			{
				number = i;
				Teleportate();
			}
	}
    public GameObject PlanetList;
    public void TeleportationButon()
    {
        if(PlanetList.gameObject.activeInHierarchy)
            PlanetList.gameObject.SetActive(false);
        else
            PlanetList.gameObject.SetActive(true);
    }


}
