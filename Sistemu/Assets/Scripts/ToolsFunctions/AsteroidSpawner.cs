﻿using UnityEngine;
using System.Collections;
using SprocketTools.ConfigShared;
using SprocketTools.Config;
using SprocketTools.MemoryPooling;

[KConfigurable]
public class AsteroidSpawner : MonoBehaviour
{
	public float spawnsPerFrame = 5;
	public float spawnsVariance = 2;

	private float spawnAccumulation;

	public GameObject templateObject;
	public string templateObjectPath;

	public Vector3 size = new Vector3(10, 10, 10);

	public Vector3 forceVector = Vector3.zero;
	public Vector3 forceVariance = Vector3.one;

	public Vector3 spawnLocation = Vector3.zero;
	public Vector3 spawnVariance = Vector3.one;

	private Vector3 spawnLocationLocal = Vector3.zero;
	private Vector3 position;

	public Transform ship = null;

	private GameObject obj = null;

	public float minScale = 0.001f;
	public float maxScale = 0.5f;

	[KConfigurableSetting("UseGameObjectPooling", true, SettingTooltip = "If true, the spawner uses memory pooling for the spawned objects.", AdvancedSetting = false)]
	public static bool UseGameObjectPooling
	{
		get
		{
			return kxcfgUseGameObjectPooling;
		}

		set
		{
			kxcfgUseGameObjectPooling = value;
			KConfig.GetConfig(typeof(AsteroidSpawner).FullName)["UseGameObjectPooling"] = value;
		}
	}

	private static bool kxcfgUseGameObjectPooling = default(bool);

	void Start()
	{

	}

	#region Methods

	void Update()
	{
		spawnAccumulation += Random.Range(spawnsPerFrame - spawnsVariance, spawnsPerFrame + spawnsVariance);

		spawnLocationLocal = transform.TransformPoint(spawnLocation);

		while (Mathf.FloorToInt(spawnAccumulation) > 0)
		{
			spawnAccumulation -= 1.0f;

			if (UseGameObjectPooling)
			{
				obj = KGameObjectPoolManager.Instantiate(templateObjectPath);
			}
			else
			{
				obj = (GameObject)GameObject.Instantiate(templateObject);
			}

			position.Set(Random.Range(spawnLocationLocal.x - spawnVariance.x, spawnLocationLocal.x + spawnVariance.x),
												Random.Range(spawnLocationLocal.y - spawnVariance.y, spawnLocationLocal.y + spawnVariance.y),
												Random.Range(spawnLocationLocal.z - spawnVariance.z, spawnLocationLocal.z + spawnVariance.z));

			obj.transform.position = position;

			obj.GetComponent<Rigidbody>().AddForce(Random.Range(forceVector.x - forceVariance.x, forceVector.x + forceVariance.x),
								Random.Range(forceVector.y - forceVariance.y, forceVector.y + forceVariance.y),
								Random.Range(forceVector.z - forceVariance.z, forceVector.z + forceVariance.z),
								ForceMode.Impulse);
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawCube(transform.position, size);
	}
	#endregion //Methods

}