﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DailyPicture : MonoBehaviour 
{
    private string url = "http://apod.nasa.gov/apod/astropix.html";

    #region Public vars
    public GameObject dayPicture;
    public RawImage dailyPicture;
    public Text title;
    public Text explanation;
    #endregion

    public void Initial () 
    {
        if (dayPicture.gameObject.activeInHierarchy == true)
            dayPicture.gameObject.SetActive(false);
        else
        {
            WWW data = new WWW(url);
           
            StartCoroutine(Wait(data));
        }
     }
     
     IEnumerator Wait(WWW data) 
     {
     
         yield return data;

        /*Picture of the day*/
         string webpage = data.text;
         string picStart ="<IMG SRC="+'"';
         string picEnd = ""+'"';
        
         string pic = "http://apod.nasa.gov/apod/"+getBetween(webpage, picStart, picEnd);
        
         WWW picture = new WWW(pic);
        
         yield return picture;
        
         dailyPicture.texture = picture.texture;



         /*Title*/
         string titleStart = "<b>";
         string titleEnd = "</b>";
         string titleText = getBetween(webpage, titleStart, titleEnd);

      //   Translation(ref titleText, FromLanguage, ToLanguage);

         title.text = titleText;


         /*Explanation*/
         string expStart = "<b> Explanation: </b>";
         string expEnd = "<p>";
         string expText = getBetween(webpage, expStart, expEnd);
        
         while (true)
         {
             string output = deleteBetween(expText, "<", ">");
             if (output != "")
             {
                 expText = output;
             }
             else
                 break;
         }

         expText = expText.Replace("\n", " ");

         explanation.text = expText;
        // yield return StartCoroutine(Translation(expText, "en", "ro"));
         dayPicture.gameObject.SetActive(true);
     }

     IEnumerator Translation(string strSource, string FromLanguage, string ToLanguage)
     {
         WWW html = new WWW("http://translate.google.com/#" + FromLanguage + "|" + ToLanguage + "|" + strSource);
         yield return html;
     //    string resultStart = "<span id=" + '"' + "result_box" + '"';
       //  string resultEnd = "</span></div>";
      //   string resultBox = getBetween(html.text, resultStart, resultEnd);
     }

     
     public string getBetween(string strSource, string strStart, string strEnd)
     {
         int Start, End;
         if (strSource.Contains(strStart) && strSource.Contains(strEnd))
         {
             Start = strSource.IndexOf(strStart, 0) + strStart.Length;
             End = strSource.IndexOf(strEnd, Start);
             return strSource.Substring(Start, End - Start);
         }
         else
         {
             return "";
         }
     }

     public string deleteBetween(string strSource, string strStart, string strEnd)
     {
         int Start, End;
         if (strSource.Contains(strStart) && strSource.Contains(strEnd))
         {
             Start = strSource.IndexOf(strStart, 0) ;
             End = strSource.IndexOf(strEnd, Start) + strEnd.Length;
             return strSource.Remove(Start, End - Start);
         }
         else
         {
             return "";
         }
     }


}