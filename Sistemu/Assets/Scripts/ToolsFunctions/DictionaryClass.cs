﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class DictionaryClass : MonoBehaviour 
{
    SortedDictionary<string, string> dictionary;
	public Button[] fields;
	public Text request;
	public Text textDefinition;
	public InputField requestField;

    void Start()
    {
		//Reset();
        XmlDocument DocumentXml = new XmlDocument();
        DocumentXml.Load(Application.streamingAssetsPath + "/Dictionar.xml");
        XmlNode root = DocumentXml.SelectSingleNode("Dictionar");

        dictionary = new SortedDictionary<string, string>();
        foreach (XmlNode node in root)
        {
			try
			{
				var name = node.Name.Replace('_', ' ');
				dictionary.Add(name, node.InnerText);
			}
			catch { Debug.Log(node.Name); }
        }
    }

	public Canvas dictionaryCanvas;
	public void ShowDictionary()
	{
		dictionaryCanvas.gameObject.SetActive(!(dictionaryCanvas.gameObject.activeInHierarchy));
		requestField.textComponent.text = null;
		Reset();
	}

	public void OnChangedQuest()
	{
		StartCoroutine(GetSuggestions());
	}

	public void PickSuggestion(Text text)
	{
		requestField.text = text.text;
		Search(text);
	}
    
	IEnumerator GetSuggestions()
    {
		Reset();
		textDefinition.text = null;
		yield return new WaitForEndOfFrame();
		string s = request.text;
		if (s != null && s!="")
		{
			var suggestions = dictionary.Keys.Where(x => x.StartsWith(s)).ToList();

			int i = 1;
			foreach (string key in suggestions)
				if(key != s)
			{
				fields[i].gameObject.SetActive(true);
				fields[i].GetComponentInChildren<Text>().text = key;
				if (i == 4)
					break;
				i++;
			}
		}
		else
			Reset();
		yield break;
    }

    public void Search(Text current)
    {
        string text = current.text;
        string definition;
		if (dictionary.TryGetValue(text, out definition))
		{
			textDefinition.text = definition;
			Reset();
		}
		else
		{
			textDefinition.text = "Din pacate, nu am gasit nici o definitie. Promitem ca in scurt timp o vom adauga!";

		}
    }

	private void Reset()
	{
		foreach (var field in fields)
			field.gameObject.SetActive(false);
	}

}
