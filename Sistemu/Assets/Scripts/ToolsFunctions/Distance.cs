﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Class Distance is used to show in the spaceship's board the distance travelled so far
/// </summary

[RequireComponent(typeof(Text))]
public class Distance : MonoBehaviour
{
    public Text target;
  
    public int distanceChangeRate = 2;

	private const int AUvalueInKm = 149597871;
    #region MonoBehaviour
  
    void Awake()
    {
        target = GetComponent<Text>();

        InvokeRepeating("ShowDistance", 0f, distanceChangeRate);
    }
    #endregion //MonoBehaviour

    #region Methods
    void ShowDistance()
    {
		if(StatsBoard.distance() > 10000000)
		{
			var distanceInAU = StatsBoard.distance()/AUvalueInKm;
			string stringFormat = System.String.Format("{0:0.#####}", distanceInAU);
			target.text =  stringFormat + " AU";
		}
		else
		{
			 target.text = ((int)StatsBoard.distance()).ToString() + " km";
		}
    }
    #endregion //Methods
}
