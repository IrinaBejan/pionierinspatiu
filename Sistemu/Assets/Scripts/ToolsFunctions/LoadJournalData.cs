﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Xml;
public class LoadJournalData : MonoBehaviour {

	public Text journalText;
	public Canvas journalBoard;
	public void ShowJournal()
	{
		if (journalBoard.gameObject.activeInHierarchy == true)
		{
			journalBoard.gameObject.SetActive(false);
		}
		else
		{
			journalBoard.gameObject.SetActive(true);
			GetContent();
		}
	}

	public static XmlDocument DocumentXml = new XmlDocument();
	public static void AddInJournal(string content)
	{
		DocumentXml.Load(Application.streamingAssetsPath + "/BoardJournal.xml");
		
		XmlNode root = DocumentXml.SelectSingleNode("Jurnal");

		var currentContent = root.InnerText;

		if ( !currentContent.Contains(content) )
		{
			root.InnerText = currentContent + content;

			DocumentXml.Save(Application.streamingAssetsPath + "/BoardJournal.xml");
		}
	}

	public void GetContent()
	{
		DocumentXml.Load(Application.streamingAssetsPath + "/BoardJournal.xml");

		XmlNode root = DocumentXml.SelectSingleNode("Jurnal");

		journalText.text = root.InnerText;
	}
}
