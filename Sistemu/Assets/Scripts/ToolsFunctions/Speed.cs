﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Class Speed is used to show in the spaceship's board the speed
/// </summary

[RequireComponent(typeof(Text))]
public class Speed : MonoBehaviour
{
    public Text target;
    #region MonoBehaviour
    void Awake()
    {
        target = GetComponent<Text>();
    }
    void Update()
    {
        target.text = StatsBoard.speedo().ToString() + " km/s";
    }
    #endregion //MonoBehaviour
}
