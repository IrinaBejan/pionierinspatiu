﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Xml;

[RequireComponent(typeof(LoadScene))]
public class ToolsButtons : MonoBehaviour
{
    #region Const
    private const string spaceObjectNode="SpaceObjects";
    private const string planetsNode="Planets";
	private const string moonsNode = "Luni";
    #endregion

	public PointsManager points;

    XmlDocument DocumentXml = new XmlDocument();
    private string spaceObjectPathDoc;
    public static string planetsPathDoc;
	private string moonsPathDoc;

    public string LastName { get; private set; }
    public string NameObj { get { return nameObj; } set{} }
    public bool Exit { get; set; }

    private string nameObj = null;
    private string tagObj = null;

    public Text text;
    public Sprite[] images;

    public bool readSomething = false;
    public CanvasGroup screenCanvasGroup;
    public Text digText;
    public Image digImage;
    public Text analysisText;
    public Text findmoreText;

    public float letterPause = 0.05f;

    string message;
    bool functionEnded = true;
    public LoadScene sceneLoader;

    void Awake()
    {
        spaceObjectPathDoc= Application.streamingAssetsPath+"/ObiecteSpatiu.xml";
       
        planetsPathDoc = Application.streamingAssetsPath + "/Planete.xml";

		moonsPathDoc = Application.streamingAssetsPath + "/Moons.xml";

        gameObject.CheckAndInitializeWithInterface<LoadScene>(ref sceneLoader);
    
        Reset();
    }

    void Update()
    {
        if (Input.GetKeyDown("escape") && screenCanvasGroup.alpha == 1)
        {
            screenCanvasGroup.alpha = 0;

            screenCanvasGroup.interactable = false;
        }
        else if (Input.GetKeyDown("escape"))
            sceneLoader.LoadNewScene(0);

        if (Input.GetKeyDown(KeyCode.T))
        {
            if(NameObj == "Mars")
                sceneLoader.LoadNewScene(3);
            if(NameObj == "Moon")
                sceneLoader.LoadNewScene(4);
            if(NameObj == "Mercury")
                sceneLoader.LoadNewScene(5);
            if(NameObj == "Venus")
                sceneLoader.LoadNewScene(6);
        }
    }

    public void RecognizeFunction()
    {
        RaycastHit hit;

        if (Physics.Raycast(this.transform.position, Vector3.forward, out hit, 1.0F))
        {
             Debug.DrawLine(transform.position, hit.point, Color.cyan);

             tagObj = hit.collider.tag;
             nameObj = hit.collider.name;
        }
    }

    IEnumerator TypeText (Text text)
    {
        functionEnded = false;
        
        if(tagObj != "SpaceObject")
            LastName = nameObj;
        screenCanvasGroup.alpha = 1;
        screenCanvasGroup.interactable = true;
       
        foreach (char letter in message.ToCharArray())
        {
            text.text += letter;
            yield return 0;
            yield return new WaitForSeconds(letterPause);
        }
    
        functionEnded = true;
        readSomething = true;
        text.gameObject.SetActive(true);
    }

    public void DigFunction()
    {
        if (functionEnded)
        {
            Reset();
            if (tagObj == "" || nameObj == "") 
                RecognizeFunction();
            if (tagObj == "Planet")
            {
                foreach(Sprite a in images)
                    if(a.name == nameObj+"_0")
                    {
                        digImage.gameObject.SetActive(true);
                        digImage.sprite = a;
                    }

                string inf = "";
                getInfo(planetsNode, nameObj + "/Sapa", planetsPathDoc, ref inf);

				string info = "";
				getInfo(planetsNode, nameObj + "/Jurnal", planetsPathDoc, ref info);
				LoadJournalData.AddInJournal(info);

                message = inf;
                StartCoroutine(TypeText(digText));
				points.AddPoints(50);
            }
            else if(tagObj == "Moon")
			{
				string inf = "";
				getInfo(moonsNode, nameObj + "/Sapa", moonsPathDoc, ref inf);

				string info = "";
				getInfo(moonsNode, nameObj + "/Jurnal", moonsPathDoc, ref info);
				LoadJournalData.AddInJournal(info);

				message = inf;
				StartCoroutine(TypeText(digText));
				points.AddPoints(50);
			}
			else
            {
                Debug.Log("No object" + tagObj);
                digText.text = "";
            }
        }
    }

    public void RepairFunction()
    {
        if(functionEnded)
        Reset();  
    }

    public void AnalysisFunction()
    {
        if (functionEnded)
        {
            Reset();
            if (tagObj == "" || nameObj == "")
                RecognizeFunction();
            if (tagObj == "Planet")
            {
                string inf = "";
                getInfo(planetsNode, nameObj + "/Analizeaza", planetsPathDoc, ref inf);
                message = inf;
                StartCoroutine(TypeText(analysisText));

				points.AddPoints(50);
            }
            else if(tagObj == "SpaceObject")
            {
                string inf = "";
                getInfo(spaceObjectNode, nameObj, spaceObjectPathDoc, ref inf);
                message = inf;
                StartCoroutine(TypeText(analysisText));

				points.AddPoints(100);
            }
			else if(tagObj == "Moon")
			{
				string inf = "";
				getInfo(moonsNode, nameObj, moonsPathDoc, ref inf);
				message = inf;
				StartCoroutine(TypeText(analysisText));

				points.AddPoints(100);
			}
            else
            {
                Debug.Log("No object" + tagObj);
                analysisText.text = "";
            }
        }
    }

    public void ProtectFunction()
    {
        if(functionEnded)
          Reset();
    }

    public void FindMoreFunction()
    {

        if (functionEnded)
        {
            Reset();
            if (tagObj == "" || nameObj == "")
                RecognizeFunction();
            if (tagObj == "Planet")
            {
                string inf = "";
                getInfo(planetsNode, nameObj + "/Istorie", planetsPathDoc, ref inf);
                message = inf;
                StartCoroutine(TypeText(findmoreText));

				points.AddPoints(100);
            }
			else if(tagObj == "Moon")
			{
				string inf = "";
				getInfo(moonsNode, nameObj + "/Istorie", moonsPathDoc, ref inf);
				message = inf;
				StartCoroutine(TypeText(findmoreText));

				points.AddPoints(100);
			}
            else
            {
                Debug.Log("No object" + tagObj);
                findmoreText.text = "";
            }
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        Exit = false;
        readSomething = false;
        tagObj = coll.gameObject.tag;
        nameObj = coll.gameObject.name;
        Debug.Log("oncolid, tag:" +tagObj+"nameObj:"+nameObj);
    }

    void OnTriggerExit(Collider coll)
    {
        Reset();
        if (readSomething == true) Exit = true;
        tagObj = null;
      //  nameObj  = null;
        readSomething = false;
        text.gameObject.SetActive(false);
    }

    void Reset()
    {
        digText.text = "";
        analysisText.text = "";
        findmoreText.text = "";
        digImage.gameObject.SetActive(false);
    }

    void getInfo(string node, string path, string pathToDoc, ref string info)
    {
        DocumentXml.Load(pathToDoc);
        XmlNode root = DocumentXml.SelectSingleNode(node);
        info = root.SelectSingleNode(path).InnerText;
    }
}
