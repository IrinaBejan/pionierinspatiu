﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Class Speed is used to show in the spaceship's board the time
/// </summary


[RequireComponent(typeof(Text))]
public class tTime : MonoBehaviour
{
    public Text target;
    public int timeChangeRate = 2;

    #region MonoBehaviour
    void Awake()
    {
        target = GetComponent<Text>();

        InvokeRepeating("ShowTime", 0f, timeChangeRate);
    }
    #endregion //MonoBehaviour

    #region Methods
    void ShowTime()
    {
		if(StatsBoard.time() > 10000)
		{
			var timeInHours = StatsBoard.time() / 3600;
			string stringFormat = System.String.Format("{0:0.###}",timeInHours);
			target.text = stringFormat + " h";
		}
		else
		{ 
			target.text = ((int)StatsBoard.time()).ToString() + " s";
		}
	}
	#endregion //Methods
}
