﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIMissionManager : MonoBehaviour
{
	public Text progressSuccesfulMissions;

	public Text progressFailedMissions;

	public Text progresNumberMissions;

	public Text progresCompletedPercentage;

	public Text progressOnGoingMissions;

	public Text progressAcceptedMissions;

	public Text progressNotAcceptedMissions;

	public void ProgressUpdate()
	{
		progresNumberMissions.text = PlayerInfo.playerData.MissionsNumber.ToString();

		progressAcceptedMissions.text = PlayerInfo.playerData.MissionsAccepted.ToString();

		progressFailedMissions.text = PlayerInfo.playerData.MissionsFailed.ToString();

		progressOnGoingMissions.text = PlayerInfo.playerData.MissionsOnGoing.ToString();

		progressSuccesfulMissions.text = PlayerInfo.playerData.MissionsCompleted.ToString();

		if (PlayerInfo.playerData.MissionsNumber != 0)
			progresCompletedPercentage.text = ((int)PlayerInfo.playerData.MissionsCompleted * 100 / PlayerInfo.playerData.MissionsNumber).ToString()+"%" ;

		progressNotAcceptedMissions.text = PlayerInfo.playerData.MissionsNotAccepted.ToString();
	}

	void Start()
	{
		if (MissionsManager.managerMissions.missionStack.Count == 0) MissionsManager.managerMissions.GetMissionsInfos();

		if (PlayerInfo.playerData.Level == 0)
			MissionsManager.managerMissions.LevelUpUpdate(0);
	}

	public Sprite[] agencies = new Sprite[5];



	public GameObject missionField;

	public GameObject parentMissionsList;
	public void Clean()
	{
		foreach (Transform child in parentMissionsList.transform)
			if (child.name.Contains("ID"))
				GameObject.Destroy(child.gameObject);
	}
	public void LoadAvailableMissions()
	{
		Clean();
		List<int> available = PlayerInfo.playerData.MissionAvailable;

		foreach (var mission in available)
		{
			var instance = Instantiate(missionField) as GameObject;

			instance.gameObject.SetActive(true);

			instance.name = "ID" + mission.ToString();

			instance.transform.SetParent(parentMissionsList.transform, true);

			instance.GetComponentsInChildren<Image>()[1].sprite = agencies[MissionsManager.managerMissions.missionStack[mission].IDImage-1];

			instance.GetComponentInChildren<Text>().text = MissionsManager.managerMissions.missionStack[mission].title;

		}
	}

	public void LoadOnGoingMissions()
	{
		Clean();
		List<int> goingOn = PlayerInfo.playerData.MissionGoingOn;

		foreach (var mission in goingOn)
		{
			var instance = Instantiate(missionField) as GameObject;

			instance.gameObject.SetActive(true);

			instance.name = "ID" + mission.ToString();

			instance.transform.SetParent(parentMissionsList.transform, false);

			instance.GetComponentsInChildren<Image>()[1].sprite = agencies[MissionsManager.managerMissions.missionStack[mission].IDImage-1];

			instance.GetComponentInChildren<Text>().text = MissionsManager.managerMissions.missionStack[mission].title;

		}
	}

	public void LoadArchiveMissions()
	{
		Clean();
		List<int> archive = PlayerInfo.playerData.MissionArchive;

		foreach (var mission in archive)
		{
			var instance = Instantiate(missionField) as GameObject;

			instance.gameObject.SetActive(true);

			instance.name = "ID" + mission.ToString();

			instance.transform.SetParent(parentMissionsList.transform, false);

			instance.GetComponentsInChildren<Image>()[1].sprite = agencies[MissionsManager.managerMissions.missionStack[mission].IDImage - 1];

			instance.GetComponentInChildren<Text>().text = MissionsManager.managerMissions.missionStack[mission].title;
		}
	}



	public Text missionTitleField;

	public Text missionDescriptionField;

	public Text missionObjectivesField;

	public Text missionRewardsField;

	public Image missionImageField;

	private string lastFilled = "";



	public void FillUIMission(GameObject missionClicked)
	{
		var index = MissionsManager.managerMissions.GetID(missionClicked.name);

		missionTitleField.text = MissionsManager.managerMissions.missionStack[index].title;

		missionDescriptionField.text = MissionsManager.managerMissions.missionStack[index].description;

		missionObjectivesField.text = MissionsManager.managerMissions.missionStack[index].objectives;

		missionRewardsField.text = MissionsManager.managerMissions.missionStack[index].rewards;

		missionImageField.sprite = agencies[MissionsManager.managerMissions.missionStack[index].IDImage-1];

		lastFilled = missionClicked.name;
	}

	public void AcceptMission()
	{
		MissionsManager.managerMissions.StartMission(lastFilled);
		LoadAvailableMissions();
	}
}
